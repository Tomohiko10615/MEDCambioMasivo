package enel.com.medcambiomasivo.application;

import java.io.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import enel.com.medcambiomasivo.utils.*;
import enel.com.medcambiomasivo.datos.clsDatos;
import enel.com.medcambiomasivo.entity.EntMedidorRetInst;
import enel.com.medcambiomasivo.entity.LineaResponse;
import enel.com.medcambiomasivo.entity.entArchivo;
import enel.com.medcambiomasivo.entity.entConexion;
import enel.com.medcambiomasivo.entity.entLectura;
import enel.com.medcambiomasivo.entity.entResultadoInt;
import enel.com.medcambiomasivo.entity.entResultadoStr;

import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.DateValidator;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.Locale;

public class MEDCambioMasivoAplicacion {

	/*
	 * private static final String url =
	 * "jdbc:postgresql://claapiapwd01.enelint.global:5432/bd_wnetwork"; private
	 * static final String usuario = "recuperos"; private static final String
	 * contrasena = "wueii_06";
	 * 
	 * private static final String urlORCL =
	 * "jdbc:oracle:thin:@cl0apmsynd00.risorse.enel:1521:EDSCPRE"; private static
	 * final String usuarioORCL = "usr_eorder"; private static final String
	 * contrasenaORCL = "transpre01$";
	 */

	private static File fileErrores;
	private static File fileResumen;

	private static String lineaResumen="";
	private static String lineaErrores="";
	
	private static final Path ROOT_PATH = FileSystems.getDefault().getRootDirectories().iterator().next();

	public static void main(String[] args) throws Exception {

		String config = ROOT_PATH + "/home/exptec/config.properties";
		System.out.println("config:" + config);
		//String config = "/home/exptec/config.properties";
		FileInputStream fis = new FileInputStream(config);
		Connection conORA = null; /* ANL */
		Connection conPG  = null; /* ANL */
		// QA               
		Properties p = new Properties();
		p.load(fis);
		// Abrir conexiones       
		String ORA_url = p.getProperty("enel.4j.configuration.InicioSesionOracle.DB_URL");
		String ORA_usr = p.getProperty("enel.4j.configuration.InicioSesionOracle.DB_USER");
		String ORA_pwd = p.getProperty("enel.4j.configuration.InicioSesionOracle.DB_PASSWORD");
		String PG_url = p.getProperty("enel.wnetwork.configuration.InicioSesionPostgresql.DB_URL");
		String PG_usr = p.getProperty("enel.wnetwork.configuration.InicioSesionPostgresql.DB_USER");
		String PG_pwd = p.getProperty("enel.wnetwork.configuration.InicioSesionPostgresql.DB_PASSWORD");

		Utilidades Util = new Utilidades();
		String passOrc = Util.Desencriptar(ORA_pwd);
		String passPgs = Util.Desencriptar(PG_pwd);
		
		if(args.length != 4)
		{
			System.out.println("Los parametros son los incorrectos");
		} else {

		String path = args[0];	
		String archivo = args[1];
		String nroODT = args[2];
		String user = args[3];
		
		//String empresa = "EDEL";
		//String user = "pe20018020";
		//String archivo = "";
		//String nroODT = "2023";

		entConexion conexOrc = new entConexion();
		entConexion conexPgs = new entConexion();

		conexOrc.setUrl(ORA_url);
		conexOrc.setUser(ORA_usr);
		conexOrc.setPassword(passOrc);
		conexPgs.setUrl(PG_url);
		conexPgs.setUser(PG_usr);
		conexPgs.setPassword(passPgs);
		FileReader fr = null;
		
		String dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a").format(LocalDateTime.now());
		System.out.println("Inicio del Proceso: " + dateTime);
		try {

			String pathMedIN = "";
			String pathMed = "";

			Integer glNroLinea = 0;
			Integer lRegProcesados = 0;
			Integer lRegActualizados = 0;
			Integer lRegRechazados = 0;

			
			BufferedReader br = null;
			Integer codEmpresa = null;
			entResultadoInt entInt;
			entResultadoStr entStr;
			Integer idusuario = 0;
			clsDatos obj;
			
			try {
				entInt = new entResultadoInt();
				obj = new clsDatos();
				entInt = obj.bfnObtenerEmpresa(PG_url, PG_usr, passPgs, "EDEL");
				if (entInt != null) {
					codEmpresa = entInt.getCampo();
				}
				entStr = new entResultadoStr();
				obj = new clsDatos();
				entStr = obj.bfnObtenerPath(PG_url, PG_usr, passPgs, "MED_REPORTES");
				if (entStr != null) {
					pathMed = entStr.getCampo();
				}
				entStr = new entResultadoStr();
				obj = new clsDatos();
				entStr = obj.bfnObtenerPath(PG_url, PG_usr, passPgs, "MED_REPORTES_IN");
				if (entStr != null) {
					pathMedIN = entStr.getCampo();
				}
				entInt = new entResultadoInt();
				obj = new clsDatos();
				entInt = obj.bfnObtenerIdUsuario(ORA_url, ORA_usr, passOrc, user);
//				if (entInt != null) {
				if (entInt.getCampo() != null ) {
					idusuario = entInt.getCampo();
				}else { /* ANL */
					System.err.printf("Usuario explotador no registrado en tabla de usuarios: [%s] ", user);
					return;
				}

				// Archivo de entrada
				String pathEntrada = "";
				String pathErrores = "";
				String pathResumen = "";

				pathEntrada = path + "/" + archivo;
				//pathEntrada = "C:\\tmp\\PROYECTOS STT\\SYNMedCambio\\testCambioMas2.txt";
//				pathEntrada = path + "\\" + archivo;

				pathErrores = Paths.get(ROOT_PATH.toString(), pathMed, "MED_CAM_MASIVO_ERRORES_" + nroODT + ".txt").toString();
		        pathResumen = Paths.get(ROOT_PATH.toString(), pathMed, "MED_CAM_MASIVO_RESUMEN_" + nroODT + ".txt").toString();
				//pathErrores = ROOT_PATH + pathMed + "/MED_CAM_MASIVO_ERRORES_" + nroODT + ".txt";
				//pathResumen = ROOT_PATH + pathMed + "/MED_CAM_MASIVO_RESUMEN_" + nroODT + ".txt"; 
				//pathErrores = "F:/tmp/salida/MED_CAM_MASIVO_ERRORES_" + nroODT + ".txt";
				//pathResumen = "F:/tmp/salida/MED_CAM_MASIVO_RESUMEN_" + nroODT + ".txt"; 
				
				System.out.println("Archivo de Entrada: " + pathEntrada);
				System.out.println("Archivo de Errores: " + pathErrores);
				System.out.println("Archivo de Resumen: " + pathResumen);

				System.out.println("Proceso Cambio Masivo de Medidores");
				
				dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a").format(LocalDateTime.now());
				System.out.println("Iniciando : " + dateTime);

				System.out.println("Proceso Cambio Masivo de Medidores");
				System.out.println("Fecha y hora inicio : " + dateTime);
				
				lineaResumen = "Proceso Cambio Masivo de Medidores \n";
				lineaResumen += "Fecha y hora inicio : " + dateTime + "\n";
				
				/* ANL */
		        conPG = DriverManager.getConnection(PG_url ,PG_usr, passPgs);		        
		        conPG.setAutoCommit(false);
		        conORA = DriverManager.getConnection(ORA_url ,ORA_usr, passOrc);		        
		        conORA.setAutoCommit(false);
				/**/
				
				System.out.println("Realizando cambio masivo de medidores...");
				
				lineaResumen += "Realizando cambio masivo de medidores...";

				fileErrores = new File(pathErrores);
				fileResumen = new File(pathResumen);

				// Crear archivos logs
				if (!fileErrores.exists()) {
					fileErrores.createNewFile();
				}
				if (!fileResumen.exists()) {
					fileResumen.createNewFile();
				}
				

				//Leyendo el archivo plano
				File file = new File(pathEntrada);
				fr = new FileReader(pathEntrada);
				br = new BufferedReader(fr);
				if (file.length() == 0) {
					System.out.println("El archivo se encuentra vacio.");
				} else {

					String linea = "";
					while ((linea = br.readLine()) != null) {
						glNroLinea++;
						lRegProcesados++;
						LineaResponse objeto = ValidarDatosDesdeLinea(linea);
						if (objeto.getCodResultado() == "OK") {

							entArchivo entidad = objeto.getListaArchivo().get(0);
							entidad.setIdempresa(codEmpresa);
							entStr = obj.bfnCambioDeMedidor(conexOrc, conexPgs, entidad, idusuario, conORA, conPG);
							if (!entStr.isResult()) {
//								lineaErrores += entStr.getCampo() + "\n";
								lineaErrores += linea +"\t Línea ["+ glNroLinea +"]. "+ entStr.getCampo() + "\n"; /* ANL */
								conPG.rollback();  /* ANL */
								conORA.rollback();
								lRegRechazados++;
							} else {
								conPG.commit();  /* ANL */
								conORA.commit();
								lRegActualizados++;
							}
						} else {
//							lineaErrores += objeto.getDesResultado() + "\n";
//							lineaErrores += "Error en la linea: " + String.valueOf(glNroLinea) + "\n";
							lineaErrores += linea +"\t Línea ["+ glNroLinea +"]. "+ objeto.getDesResultado() + "\n"; /* ANL */
							
							lRegRechazados++;
						}
						objeto = null;

					}

					dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a").format(LocalDateTime.now());
					lineaResumen += "Fecha y hora fin : " + dateTime + "\n";
					lineaResumen += "Archivo de Entrada: " + pathEntrada + "\n";
					lineaResumen += "Archivo de Errores: " + pathErrores + "\n";
					lineaResumen += "Archivo de Resumen: " + pathResumen + "\n";
					lineaResumen += "Cantidad Registros Procesados  : " + lRegProcesados + "\n";
					lineaResumen += "Cantidad Registros Actualizados: " + lRegActualizados + "\n";
					lineaResumen += "Cantidad Registros Rechazados  : " + lRegRechazados + "\n";
					
					System.out.println("Fecha y hora fin : " + dateTime);
					System.out.println("Archivo de Entrada: " + pathEntrada);
					System.out.println("Archivo de Errores: " + pathErrores);
					System.out.println("Archivo de Resumen: " + pathResumen);
					System.out.println("Cantidad Registros Procesados  : " + lRegProcesados);
					System.out.println("Cantidad Registros Actualizados: " + lRegActualizados);
					System.out.println("Cantidad Registros Rechazados  : " + lRegRechazados);

					//Escribiendo el el log de resumen
					if (lineaResumen != null && lineaResumen.length() > 0) {
						FileWriter fwR = new FileWriter(fileResumen);
			            BufferedWriter bwR = new BufferedWriter(fwR);
			            bwR.write(lineaResumen);
			            bwR.close();
			            
						//Respaldar archivo en el SFTP // MOD MA 110523
						if(obj.Activo_SFPT(conexPgs).equals("A")){
							String dirSFTP = obj.Ruta_Directorio_SFPT(conexPgs);
							respaldarArchivo(fileResumen.getPath(), dirSFTP, config);
						}
					}
		            
		            //Escribiendo en el log de errores
					if (lineaErrores != null && lineaErrores.length() > 0) {						
						FileWriter fwX = new FileWriter(fileErrores);
			            BufferedWriter bwX = new BufferedWriter(fwX);
			            bwX.write(lineaErrores);
			            bwX.close();
			            
			            
						//Respaldar archivo en el SFTP // MOD MA 110523
						if(obj.Activo_SFPT(conexPgs).equals("A")){
							String dirSFTP = obj.Ruta_Directorio_SFPT(conexPgs);
							respaldarArchivo(fileErrores.getPath(), dirSFTP, config);
						}
					}
				}

				dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a").format(LocalDateTime.now());
				System.out.println("Salida del Proceso:" + dateTime);

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Error de APP: " + e.getMessage());
			}

			dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a").format(LocalDateTime.now());
			System.out.println("Fin del Proceso: " + dateTime);

		} catch (Exception ex) {
			System.err.println("Error de APP: " + ex.getMessage());
		}
		finally {
			try {
				if (null != fr) {fr.close(); }
			} catch (Exception e2) { e2.printStackTrace();}
							
    		if( conPG != null ) {  /* ANL */
    			try { conPG.close(); } catch (SQLException e) {e.printStackTrace();}
    		}  		
    		if( conORA != null ) {
    			try { conORA.close(); } catch (SQLException e) {e.printStackTrace();}
    		}

    		System.exit(0); /* ANL */
		}


		} //endif args

	}

	private static LineaResponse ValidarDatosDesdeLinea(String Linea) {
		final int TOTAL_CAMPOS_MIN = 27;
		final int TOTAL_CAMPOS_FIJOS = 11;
		final int TOTAL_CAMPOS_MEDIDORES_RET_INST_MIN = 16;
		LineaResponse list = new LineaResponse();
		entArchivo entidad = new entArchivo();
		ArrayList<entLectura> entLctRet = new ArrayList<entLectura>();
		ArrayList<entLectura> entLctInst = new ArrayList<entLectura>();
		boolean booLinea = true;
		String columna = "";
		int lecturas = 0;
		ArrayList<entArchivo> listaArchivo = new ArrayList();
		try {
			list.setCodResultado("OK");
			list.setDesResultado("");

			if (Linea.length() == 0) {
				list.setCodResultado("KO");
				list.setDesResultado("La linea se encuentra vacia.");
			} else {

				System.out.println("Linea: " + Linea);
				String[] objOrden = Linea.split("\\s+");
				System.out.println("objOrden length: " + objOrden.length);
				// R.I. REQSCOM10 02/08/2023 INICIO
				if (objOrden.length >= TOTAL_CAMPOS_MIN 
						&& (objOrden.length - TOTAL_CAMPOS_FIJOS) % TOTAL_CAMPOS_MEDIDORES_RET_INST_MIN == 0) {
					entidad.setCodContratista(objOrden[0]);
					if (!bfnHayValor(objOrden[0])) {
						columna = "Contratista";
						booLinea = false;
					}
	
					entidad.setCodEjecutor("");
					entidad.setNroCuenta(objOrden[1]);
					if (bfnHayValor(objOrden[1])) {
						if (!EsNumero(objOrden[1])) {
							if (columna.length() == 0) {
								columna = "Cuenta ";
							} else {
								columna += ",Cuenta ";
							}
							booLinea = false;
						}
					}
	
					entidad.setFecLectura(objOrden[2]);
					if (bfnHayValor(objOrden[2])) {
						if (!esFecha(objOrden[2])) {
							if (columna.length() == 0) {
								columna = "Fecha Lectura ";
							} else {
								columna += ",Fecha Lectura ";
							}
							booLinea = false;
						}
					}
					
					List<EntMedidorRetInst> medidores = new ArrayList<>();
					
					int cantMedidores = (objOrden.length - TOTAL_CAMPOS_FIJOS) / TOTAL_CAMPOS_MEDIDORES_RET_INST_MIN;
					int position = 3;
					
					for (int i = 0; i < cantMedidores; i++) {
						entLctRet = new ArrayList<entLectura>();
						entLctInst = new ArrayList<entLectura>();
						EntMedidorRetInst medidorRetInst = new EntMedidorRetInst();
						medidorRetInst.setMarcaRetiro(objOrden[position]);
						if (!bfnHayValor(objOrden[position])) {
							if (columna.length() == 0) {
								columna = "Marca Retiro ";
							} else {
								columna += ",Marca Retiro ";
							}
							booLinea = false;
						}
						
						position = position + 1;
						
						if (i == 0 && (medidorRetInst.getMarcaRetiro().equals("0"))) {
							if (columna.length() == 0) {
								columna = "Primer medidor a retirar ";
							} else {
								columna += ",Primer medidor a retirar ";
							}
							booLinea = false;
						}
						
						if (!medidorRetInst.getMarcaRetiro().equals("0")) {
							medidorRetInst.setModeloRetiro(objOrden[position]);
							if (!bfnHayValor(objOrden[position])) {
								if (columna.length() == 0) {
									columna = "Modelo Retiro ";
								} else {
									columna += ",Modelo Retiro ";
								}
								booLinea = false;
							}
							position = position + 1;
			
							medidorRetInst.setNroMedidorRet(objOrden[position]);
							if (!bfnHayValor(objOrden[position])) {
								if (columna.length() == 0) {
									columna = "Numero Medidor Retiro ";
								} else {
									columna += ",Numero Medidor Retiro ";
								}
								booLinea = false;
							}
							position = position + 1;
			
							// Descomponiendo lecturaRetiro
							lecturas = 5;
							while (lecturas > 0) {
								String lecturaRetiro = objOrden[position];
								if (lecturaRetiro.equals("") || lecturaRetiro.equals("0")) {
									position = position + 1;
									lecturas--;
									continue;
								}
								String[] arrLctRet = lecturaRetiro.split("\\*");
								entLectura lctRet = new entLectura();
								lctRet.setNroCuenta(objOrden[1]);
								lctRet.setCodigo(arrLctRet[0]);
								lctRet.setValor(arrLctRet[1]);
								entLctRet.add(lctRet);
								
								//medidorRetInst.setLectACFPretiro(arrLctRet[1]);
								if (bfnHayValor(lecturaRetiro)) {
									if (!EsNumero(arrLctRet[1])) {
										if (columna.length() == 0) {
											columna = "Lectura Retiro ";
										} else {
											columna += ",Lectura Retiro ";
										}
										booLinea = false;
									}
								}
								position = position + 1;
								lecturas--;
							}
							
							medidorRetInst.setLstLectRet(entLctRet);
						} else {
							position = position + 7;
						}
						
						medidorRetInst.setMarcaInstalar(objOrden[position]);
						if (!bfnHayValor(objOrden[position])) {
							if (columna.length() == 0) {
								columna = "Marca Instalar ";
							} else {
								columna += ",Marca Instalar ";
							}
							booLinea = false;
						}
						position = position + 1;
						
						if (i == 0 && (medidorRetInst.getMarcaInstalar().equals("0"))) {
							if (columna.length() == 0) {
								columna = "Primer medidor a instalar ";
							} else {
								columna += ",Primer medidor a instalar ";
							}
							booLinea = false;
						}
						
						if (!medidorRetInst.getMarcaInstalar().equals("0")) {
							medidorRetInst.setModeloInstalar(objOrden[position]);
							if (!bfnHayValor(objOrden[position])) {
								if (columna.length() == 0) {
									columna = "Modelo Instalar ";
								} else {
									columna += ",Modelo Instalar ";
								}
								booLinea = false;
							}
							position = position + 1;
			
							medidorRetInst.setNroMedidorInstalar(objOrden[position]);
							if (!bfnHayValor(objOrden[position])) {
								if (columna.length() == 0) {
									columna = "Numero Medidor Instalar ";
								} else {
									columna += ",Numero Medidor Instalar ";
								}
								booLinea = false;
							}
							position = position + 1;
			
							// Descomponiendo lecturaInstlar
							lecturas = 5;
							while (lecturas > 0) {
								String lecturaInstalar = objOrden[position];
								if (lecturaInstalar.equals("") || lecturaInstalar.equals("0")) {
									position = position + 1;
									lecturas--;
									continue;
								}
								String[] arrLctIns = lecturaInstalar.split("\\*");
								entLectura lctIns = new entLectura();
								lctIns.setNroCuenta(objOrden[1]);
								lctIns.setCodigo(arrLctIns[0]);
								lctIns.setValor(arrLctIns[1]);
								entLctInst.add(lctIns);
								//medidorRetInst.setLectACFPinstalar(arrLctIns[1]);
								if (bfnHayValor(lecturaInstalar)) {
									if (!EsNumero(arrLctIns[1])) {
										if (columna.length() == 0) {
											columna = "Lectura Instalar ";
										} else {
											columna += ",Lectura Instalar ";
										}
										booLinea = false;
									}
								}
								position = position + 1;
								lecturas--;
							}
							medidorRetInst.setLstLectIns(entLctInst);
						} else {
							position = position + 7;
						}
						medidores.add(medidorRetInst);
						System.out.println("Par de medidores: " + medidorRetInst.toString());
					}
					
					entidad.setFactorInstalar(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Factor Medidor Instalar ";
						} else {
							columna += ",Factor Medidor Instalar ";
						}
						booLinea = false;
					}
					position = position + 1;
					
					entidad.setCodPropiedad(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Propiedad ";
						} else {
							columna += ",Propiedad ";
						}
						booLinea = false;
					}
					position = position + 1;
	
					entidad.setNroSello1(objOrden[position]);
					if (bfnHayValor(objOrden[position])) {
						if (!EsNumero(objOrden[position])) {
							if (columna.length() == 0) {
								columna = "Nro.Sello 1 ";
							} else {
								columna += ",Nro.Sello 1 ";
							}
							booLinea = false;
						}
					}
					position = position + 1;
	
					entidad.setColorSello1(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Color Sello 1 ";
						} else {
							columna += ",Color Sello 1 ";
						}
						booLinea = false;
					}
					position = position + 1;
	
					entidad.setUbicacionSello1(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Ubicacion Sello 1 ";
						} else {
							columna += ",Ubicacion Sello 1 ";
						}
						booLinea = false;
					}
					position = position + 1;
					entidad.setNroSello2(objOrden[position]);
					if (bfnHayValor(objOrden[position])) {
						if (!EsNumero(objOrden[position])) {
							if (columna.length() == 0) {
								columna = "Nro.Sello 2 ";
							} else {
								columna += ",Nro.Sello 2 ";
							}
							booLinea = false;
						}
					}
					position = position + 1;
	
					entidad.setColorSello2(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Color Sello 2 ";
						} else {
							columna += ",Color Sello 2 ";
						}
						booLinea = false;
					}
					position = position + 1;
	
					entidad.setUbicacionSello2(objOrden[position]);
					if (!bfnHayValor(objOrden[position])) {
						if (columna.length() == 0) {
							columna = "Ubicacion Sello 2 ";
						} else {
							columna += ",Ubicacion Sello 2 ";
						}
						booLinea = false;
					}
					// R.I. REQSCOM10 02/08/2023 FIN
					
					entidad.setMedidores(medidores);
					
					if (!booLinea) {
						list.setCodResultado("KO");
						list.setDesResultado("La(s) sgte(s) columna(s) son dato(s) obligatorio(s) : " + columna);
						lineaErrores = columna;
					} else {
						listaArchivo.add(entidad);
						list.setListaArchivo(listaArchivo);
					}
				
				} //endif length
				else {
					list.setCodResultado("KO");
					list.setDesResultado("La linea no tiene la cantidad correcta de valores");
				}

			}

		} catch (Exception e) {
			System.err.println("Error al validar la linea - " + e.getMessage());
			list.setCodResultado("KO");
			list.setDesResultado("Ocurrio un error al validad la linea - " + e.getMessage());
		}

		return list;
	}

	public static boolean bfnHayValor(String valor) {

		if (valor != null) {
			if (valor.length() == 0) {
				return false;
			}
		} else {
			return false;
		}

		return true;
	}

	public static boolean EsNumero(String valor) {

		if (valor != null) {
			boolean isNumeric = (valor != null && valor.matches("[0-9]+"));
			return isNumeric;
		} else {
			return false;
		}

	}

	public static boolean esFecha(String date) {
		
		boolean result;
        try {
        	String DATE_FORMAT = "dd/MM/yyyy";
        	DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        	 df.setLenient(false);
             df.parse(date);
            result = true;
        } catch (ParseException e) {
            result = false;
        }
        return result;
	}

	public static boolean dateValidator(String date) {
		// Obtener el validador de fecha
		DateValidator validator = DateValidator.getInstance();
		// Validar la fecha
		return validator.validate(date, Locale.ENGLISH) != null;
	}
	
	
	/***/
	public static Boolean respaldarArchivo(String pRutaArchivo, String pDirSFTP, String pConfig) throws Exception{
    	
    	FileInputStream fis = null;
    	Utilidades Util = new Utilidades();
    	Boolean resp= false;

        fis = new FileInputStream(pConfig); 

        Properties p = new Properties();
        p.load(fis);
        
    	//--
        
        System.out.println(LocalDateTime.now() + "| Obtener datos del SFTP");
        
		String host = p.getProperty("server.ftp.sc.host");
		Integer port = Integer.parseInt(p.getProperty("server.ftp.sc.port"));
		String user = p.getProperty("server.ftp.sc.user");
		String pass = Util.Desencriptar(p.getProperty("server.ftp.sc.pass"));
		String remote_dir_path = pDirSFTP; //p.getProperty("server.ftp.sc.remote_dir_path");
		
    	try {
    		
    		
    		JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
		    session.setConfig("StrictHostKeyChecking", "no");
		    session.setPassword(pass);
		    session.connect();
		    System.out.println(LocalDateTime.now() + "| Conexión SFTP establecida. Host: " + host );
	        
		

		    Channel channel = session.openChannel("sftp");
		    channel.connect();
		    ChannelSftp sftpChannel = (ChannelSftp) channel;
		    System.out.println(LocalDateTime.now() + "| Crear canal SFTP");
		    
		    sftpChannel.cd(remote_dir_path);
		    File archivo = new File (pRutaArchivo);
		    ByteArrayInputStream bis = new ByteArrayInputStream(FileUtils.readFileToByteArray(archivo));
		    sftpChannel.put(bis, archivo.getName());
		    System.out.println(LocalDateTime.now() + "| Copiando archivo al SFTP: " + archivo.getName());
    		
		    resp = true;
    		
    		
    	}catch (Exception e){
	    	e.printStackTrace();
	    	System.err.println(String.format("Error al respaldar archivo en el SFTP. [%s] / [%s]", pRutaArchivo  , e.getMessage()));
		} finally {
		    fis.close();
		}
    	
    	return resp;
    	
    }


}
