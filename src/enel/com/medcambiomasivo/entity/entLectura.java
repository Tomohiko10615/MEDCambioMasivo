package enel.com.medcambiomasivo.entity;

public class entLectura {
	
	private String nroCuenta; 
	private String codigo;
	private String valor;
	
	@Override
	public String toString() {
		return "entLectura [nroCuenta=" + nroCuenta + ", codigo=" + codigo + ", valor=" + valor + "]";
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}