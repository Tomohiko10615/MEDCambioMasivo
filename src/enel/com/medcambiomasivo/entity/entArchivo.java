package enel.com.medcambiomasivo.entity;

import java.util.List;

public class entArchivo {

	private Integer idempresa;
	private String codContratista;
	private String codEjecutor;
	private String nroCuenta;
	private String fecLectura;
	// R.I. REQSCOM11 01/08/2023 INICIO
	private List<EntMedidorRetInst> medidores;
	private String factorInstalar;
	/*
	private String marcaRetiro;
	private String modeloRetiro;
	private String nroMedidorRet;
	private String lectACFPretiro;
	private String marcaInstalar;
	private String modeloInstalar;
	private String nroMedidorInstalar;
	private String lectACFPinstalar;
	public List<entLectura> lstLectRet;
	public List<entLectura> lstLectIns;
	*/
	// R.I. REQSCOM11 01/08/2023 FIN
	private String codPropiedad;
	private String nroSello1;
	private String colorSello1;
	private String ubicacionSello1;
	private String nroSello2;
	private String colorSello2;
	private String ubicacionSello2;
	
	public Integer getIdempresa() {
		return idempresa;
	}
	public void setIdempresa(Integer idempresa) {
		this.idempresa = idempresa;
	}
	public String getCodContratista() {
		return codContratista;
	}
	public void setCodContratista(String codContratista) {
		this.codContratista = codContratista;
	}
	public String getCodEjecutor() {
		return codEjecutor;
	}
	public void setCodEjecutor(String codEjecutor) {
		this.codEjecutor = codEjecutor;
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getFecLectura() {
		return fecLectura;
	}
	public void setFecLectura(String fecLectura) {
		this.fecLectura = fecLectura;
	}
	// R.I. REQSCOM11 01/08/2023 INICIO
	/*
	public String getMarcaRetiro() {
		return marcaRetiro;
	}
	public void setMarcaRetiro(String marcaRetiro) {
		this.marcaRetiro = marcaRetiro;
	}
	public String getModeloRetiro() {
		return modeloRetiro;
	}
	public void setModeloRetiro(String modeloRetiro) {
		this.modeloRetiro = modeloRetiro;
	}
	public String getNroMedidorRet() {
		return nroMedidorRet;
	}
	public void setNroMedidorRet(String nroMedidorRet) {
		this.nroMedidorRet = nroMedidorRet;
	}
	public String getLectACFPretiro() {
		return lectACFPretiro;
	}
	public void setLectACFPretiro(String lectACFPretiro) {
		this.lectACFPretiro = lectACFPretiro;
	}
	public String getMarcaInstalar() {
		return marcaInstalar;
	}
	public void setMarcaInstalar(String marcaInstalar) {
		this.marcaInstalar = marcaInstalar;
	}
	public String getModeloInstalar() {
		return modeloInstalar;
	}
	public void setModeloInstalar(String modeloInstalar) {
		this.modeloInstalar = modeloInstalar;
	}
	public String getNroMedidorInstalar() {
		return nroMedidorInstalar;
	}
	public void setNroMedidorInstalar(String nroMedidorInstalar) {
		this.nroMedidorInstalar = nroMedidorInstalar;
	}
	public String getLectACFPinstalar() {
		return lectACFPinstalar;
	}
	public void setLectACFPinstalar(String lectACFPinstalar) {
		this.lectACFPinstalar = lectACFPinstalar;
	}
	public List<entLectura> getLstLectRet() {
		return lstLectRet;
	}
	public void setLstLectRet(List<entLectura> lstLectRet) {
		this.lstLectRet = lstLectRet;
	}
	public List<entLectura> getLstLectIns() {
		return lstLectIns;
	}
	public void setLstLectIns(List<entLectura> lstLectIns) {
		this.lstLectIns = lstLectIns;
	}
	*/
	// R.I. REQSCOM11 01/08/2023 INICIO
	public String getCodPropiedad() {
		return codPropiedad;
	}
	public void setCodPropiedad(String codPropiedad) {
		this.codPropiedad = codPropiedad;
	}
	public String getNroSello1() {
		return nroSello1;
	}
	public void setNroSello1(String nroSello1) {
		this.nroSello1 = nroSello1;
	}
	public String getColorSello1() {
		return colorSello1;
	}
	public void setColorSello1(String colorSello1) {
		this.colorSello1 = colorSello1;
	}
	public String getUbicacionSello1() {
		return ubicacionSello1;
	}
	public void setUbicacionSello1(String ubicacionSello1) {
		this.ubicacionSello1 = ubicacionSello1;
	}
	public String getNroSello2() {
		return nroSello2;
	}
	public void setNroSello2(String nroSello2) {
		this.nroSello2 = nroSello2;
	}
	public String getColorSello2() {
		return colorSello2;
	}
	public void setColorSello2(String colorSello2) {
		this.colorSello2 = colorSello2;
	}
	public String getUbicacionSello2() {
		return ubicacionSello2;
	}
	public void setUbicacionSello2(String ubicacionSello2) {
		this.ubicacionSello2 = ubicacionSello2;
	}
	public List<EntMedidorRetInst> getMedidores() {
		return medidores;
	}
	public void setMedidores(List<EntMedidorRetInst> medidores) {
		this.medidores = medidores;
	}
	public String getFactorInstalar() {
		return factorInstalar;
	}
	public void setFactorInstalar(String factorInstalar) {
		this.factorInstalar = factorInstalar;
	}
	
}


