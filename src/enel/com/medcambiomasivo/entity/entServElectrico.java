package enel.com.medcambiomasivo.entity;

public class entServElectrico {

	private String idstate;
	private String idestado;
	private Integer idservicio;
	
	public String getIdstate() {
		return idstate;
	}
	public void setIdstate(String idstate) {
		this.idstate = idstate;
	}
	public String getIdestado() {
		return idestado;
	}
	public void setIdestado(String idestado) {
		this.idestado = idestado;
	}
	public Integer getIdservicio() {
		return idservicio;
	}
	public void setIdservicio(Integer idservicio) {
		this.idservicio = idservicio;
	}
	
}
