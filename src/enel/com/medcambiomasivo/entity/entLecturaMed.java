package enel.com.medcambiomasivo.entity;

public class entLecturaMed {

	private boolean result;
	private Long id_componente;
	private String cod_medida;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Long getId_componente() {
		return id_componente;
	}
	public void setId_componente(Long id_componente) {
		this.id_componente = id_componente;
	}
	public String getCod_medida() {
		return cod_medida;
	}
	public void setCod_medida(String cod_medida) {
		this.cod_medida = cod_medida;
	}
	
	
}
