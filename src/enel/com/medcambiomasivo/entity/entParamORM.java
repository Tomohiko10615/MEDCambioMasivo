package enel.com.medcambiomasivo.entity;

public class entParamORM {

	Integer id_motivo;
	Integer id_buzon;
	Integer id_trabajo;
	Integer id_prioridad;
	Integer id_tema;
	Integer id_responsable;
	Integer id_tarea;
	String  Observacion;
	
	
	public Integer getId_motivo() {
		return id_motivo;
	}
	public void setId_motivo(Integer id_motivo) {
		this.id_motivo = id_motivo;
	}
	public Integer getId_buzon() {
		return id_buzon;
	}
	public void setId_buzon(Integer id_buzon) {
		this.id_buzon = id_buzon;
	}
	public Integer getId_trabajo() {
		return id_trabajo;
	}
	public void setId_trabajo(Integer id_trabajo) {
		this.id_trabajo = id_trabajo;
	}
	public Integer getId_prioridad() {
		return id_prioridad;
	}
	public void setId_prioridad(Integer id_prioridad) {
		this.id_prioridad = id_prioridad;
	}
	public Integer getId_tema() {
		return id_tema;
	}
	public void setId_tema(Integer id_tema) {
		this.id_tema = id_tema;
	}
	public Integer getId_responsable() {
		return id_responsable;
	}
	public void setId_responsable(Integer id_responsable) {
		this.id_responsable = id_responsable;
	}
	public Integer getId_tarea() {
		return id_tarea;
	}
	public void setId_tarea(Integer id_tarea) {
		this.id_tarea = id_tarea;
	}
	public String getObservacion() {
		return Observacion;
	}
	public void setObservacion(String observacion) {
		Observacion = observacion;
	}
	
	
}
