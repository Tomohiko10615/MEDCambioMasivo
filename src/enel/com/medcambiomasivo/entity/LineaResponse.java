package enel.com.medcambiomasivo.entity;

import java.util.List;

public class LineaResponse {

	private String codResultado;
	private String desResultado;
	private List<entArchivo> ListaArchivo;
	
	public String getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(String codResultado) {
		this.codResultado = codResultado;
	}
	public String getDesResultado() {
		return desResultado;
	}
	public void setDesResultado(String desResultado) {
		this.desResultado = desResultado;
	}
	public List<entArchivo> getListaArchivo() {
		return ListaArchivo;
	}
	public void setListaArchivo(List<entArchivo> listaArchivo) {
		ListaArchivo = listaArchivo;
	}	
	
}
