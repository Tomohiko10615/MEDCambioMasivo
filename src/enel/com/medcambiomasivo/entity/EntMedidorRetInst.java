// R.I. REQSCOM11 01/08/2023 INICIO
package enel.com.medcambiomasivo.entity;

import java.util.List;

public class EntMedidorRetInst {

	@Override
	public String toString() {
		return "EntMedidorRetInst [marcaRetiro=" + marcaRetiro + ", modeloRetiro=" + modeloRetiro + ", nroMedidorRet="
				+ nroMedidorRet + ", lectACFPretiro=" + lectACFPretiro + ", marcaInstalar=" + marcaInstalar
				+ ", modeloInstalar=" + modeloInstalar + ", nroMedidorInstalar=" + nroMedidorInstalar
				+ ", lectACFPinstalar=" + lectACFPinstalar + ", lstLectRet=" + lstLectRet + ", lstLectIns=" + lstLectIns
				+ "]";
	}
	private String marcaRetiro;
	private String modeloRetiro;
	private String nroMedidorRet;
	private String lectACFPretiro;
	private String marcaInstalar;
	private String modeloInstalar;
	private String nroMedidorInstalar;
	private String lectACFPinstalar;
	public List<entLectura> lstLectRet;
	public List<entLectura> lstLectIns;
	public List<entLectura> getLstLectRet() {
		return lstLectRet;
	}
	public void setLstLectRet(List<entLectura> lstLectRet) {
		this.lstLectRet = lstLectRet;
	}
	public List<entLectura> getLstLectIns() {
		return lstLectIns;
	}
	public void setLstLectIns(List<entLectura> lstLectIns) {
		this.lstLectIns = lstLectIns;
	}
	
	public String getMarcaRetiro() {
		return marcaRetiro;
	}
	public void setMarcaRetiro(String marcaRetiro) {
		this.marcaRetiro = marcaRetiro;
	}
	public String getModeloRetiro() {
		return modeloRetiro;
	}
	public void setModeloRetiro(String modeloRetiro) {
		this.modeloRetiro = modeloRetiro;
	}
	public String getNroMedidorRet() {
		return nroMedidorRet;
	}
	public void setNroMedidorRet(String nroMedidorRet) {
		this.nroMedidorRet = nroMedidorRet;
	}
	public String getLectACFPretiro() {
		return lectACFPretiro;
	}
	public void setLectACFPretiro(String lectACFPretiro) {
		this.lectACFPretiro = lectACFPretiro;
	}
	public String getMarcaInstalar() {
		return marcaInstalar;
	}
	public void setMarcaInstalar(String marcaInstalar) {
		this.marcaInstalar = marcaInstalar;
	}
	public String getModeloInstalar() {
		return modeloInstalar;
	}
	public void setModeloInstalar(String modeloInstalar) {
		this.modeloInstalar = modeloInstalar;
	}
	public String getNroMedidorInstalar() {
		return nroMedidorInstalar;
	}
	public void setNroMedidorInstalar(String nroMedidorInstalar) {
		this.nroMedidorInstalar = nroMedidorInstalar;
	}
	public String getLectACFPinstalar() {
		return lectACFPinstalar;
	}
	public void setLectACFPinstalar(String lectACFPinstalar) {
		this.lectACFPinstalar = lectACFPinstalar;
	}
	
}
// R.I. REQSCOM11 01/08/2023 FIN
