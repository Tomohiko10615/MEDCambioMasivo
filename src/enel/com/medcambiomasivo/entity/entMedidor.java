package enel.com.medcambiomasivo.entity;

public class entMedidor {
 
	private String seESTADO;
	private String seIDESTADO;
	private Integer seIdServicioRet;
	private String fechaEjecuVta;
	private Integer idMedMarca;
	private Integer idMedModeloRet;
	private Integer idMedModeloIns;
	private String colorSello;
	
	public String getSeESTADO() {
		return seESTADO;
	}
	public void setSeESTADO(String seESTADO) {
		this.seESTADO = seESTADO;
	}
	public String getSeIDESTADO() {
		return seIDESTADO;
	}
	public void setSeIDESTADO(String seIDESTADO) {
		this.seIDESTADO = seIDESTADO;
	}
	public Integer getSeIdServicioRet() {
		return seIdServicioRet;
	}
	public void setSeIdServicioRet(Integer seIdServicioRet) {
		this.seIdServicioRet = seIdServicioRet;
	}
	public String getFechaEjecuVta() {
		return fechaEjecuVta;
	}
	public void setFechaEjecuVta(String fechaEjecuVta) {
		this.fechaEjecuVta = fechaEjecuVta;
	}
	public Integer getIdMedMarca() {
		return idMedMarca;
	}
	public void setIdMedMarca(Integer idMedMarca) {
		this.idMedMarca = idMedMarca;
	}
	public Integer getIdMedModeloRet() {
		return idMedModeloRet;
	}
	public void setIdMedModeloRet(Integer idMedModeloRet) {
		this.idMedModeloRet = idMedModeloRet;
	}
	public Integer getIdMedModeloIns() {
		return idMedModeloIns;
	}
	public void setIdMedModeloIns(Integer idMedModeloIns) {
		this.idMedModeloIns = idMedModeloIns;
	}
	public String getColorSello() {
		return colorSello;
	}
	public void setColorSello(String colorSello) {
		this.colorSello = colorSello;
	}

	
	
}
