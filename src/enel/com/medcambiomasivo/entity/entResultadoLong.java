package enel.com.medcambiomasivo.entity;

public class entResultadoLong {

	private boolean result;
	private Long campo;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Long getCampo() {
		return campo;
	}
	public void setCampo(Long campo) {
		this.campo = campo;
	}
	
	
}
