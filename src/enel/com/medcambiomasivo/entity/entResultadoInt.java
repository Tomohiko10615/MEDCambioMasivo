package enel.com.medcambiomasivo.entity;

public class entResultadoInt {

	private boolean result;
	private Integer campo;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Integer getCampo() {
		return campo;
	}
	public void setCampo(Integer campo) {
		this.campo = campo;
	}
	
	
}
