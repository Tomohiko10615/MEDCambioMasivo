package enel.com.medcambiomasivo.entity;

public class entResultadoStr {

	private boolean result;
	private String campo;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	
}
