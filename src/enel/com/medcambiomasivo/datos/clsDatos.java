package enel.com.medcambiomasivo.datos;

import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.crypto.dsig.spec.XPathFilterParameterSpec;

import enel.com.medcambiomasivo.entity.EntMedidorRetInst;
import enel.com.medcambiomasivo.entity.entArchivo;
import enel.com.medcambiomasivo.entity.entConexion;
import enel.com.medcambiomasivo.entity.entLectura;
import enel.com.medcambiomasivo.entity.entLecturaMed;
import enel.com.medcambiomasivo.entity.entParamORM;
import enel.com.medcambiomasivo.entity.entResultadoInt;
import enel.com.medcambiomasivo.entity.entResultadoLong;
import enel.com.medcambiomasivo.entity.entResultadoStr;
import enel.com.medcambiomasivo.entity.entServElectrico;

public class clsDatos {

	public entResultadoInt bfnObtenerEmpresa(String url, String user, String pass, String codEmpresa) throws SQLException {
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select id_empresa FROM schscom.nuc_empresa Where cod_partition = '" + codEmpresa + "' ";
			ResultSet rsUser = st.executeQuery(sql);
			while (rsUser.next()) {
				objInt = new entResultadoInt();
				int id = rsUser.getInt("id_empresa");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al obtener el ID de la empresa. Revisar Log del Proceso.");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnObtenerIdUsuario(String url, String user, String pass, String usuario) 
			throws SQLException {
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select id_usuario FROM synergia.seg_usuario Where UPPER(USERNAME) = UPPER('" + usuario + "') ";
			//System.out.println(sql);
			ResultSet rsUser = st.executeQuery(sql);
			while (rsUser.next()) {
				int iduser = rsUser.getInt("id_usuario");
				System.out.println("iduser: "+iduser);
				objInt.setResult(true);
				objInt.setCampo(iduser);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al obtener el ID del Usuario. Revisar Log del Proceso" + ex.getMessage());
			objInt.setResult(false);
			objInt.setCampo(null);
			ex.printStackTrace();
		} catch (Exception e) {
			st.close();
			BaseDatos.close();
			e.printStackTrace();
			System.err.println("Error de bfnObtenerIdUsuario: " + e.getMessage());
		}
		return objInt;
	}
	
	public entResultadoStr bfnObtenerPath(String url, String user, String pass, String key) throws SQLException {
		Connection BaseDatos = null;
		Statement st = null;
		String rutaPath = "";
		entResultadoStr objStr = new entResultadoStr();
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			ResultSet rs = st.executeQuery("Select path FROM SCHSCOM.COM_PATH Where KEY ='" +key+"' ");// and id_empresa =
			// " +
			// String.valueOf(idemp));
			while (rs.next()) {
				rutaPath = rs.getString("path");
				System.out.println(rutaPath);
				objStr.setResult(true);
				objStr.setCampo(rutaPath);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al obtener la ruta de archivos : " + key);
			objStr.setResult(false);
			objStr.setCampo(null);
		}
		return objStr;
	}
	
	public entResultadoInt bfnExisteCuenta(String url, String user, String pass, Integer pIDEmpresa, 
			long pCuenta) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select count(1) reg FROM SYNERGIA.NUC_CUENTA Where id_empresa = " + String.valueOf(pIDEmpresa) + " and nro_cuenta = " + String.valueOf(pCuenta);
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int idemp = rs.getInt("reg");
				System.out.println(idemp);
				objInt.setResult(true);
				objInt.setCampo(idemp);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia de la Cuenta " + String.valueOf(pCuenta));
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteMedInstal(String url, String user, String pass, long pCuenta) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select count(1) reg FROM SYNERGIA.NUC_CUENTA NC, SYNERGIA.NUC_SERVICIO NS WHERE NC.NRO_CUENTA = " + String.valueOf(pCuenta) + 
					" AND NC.ID_CUENTA = NS.ID_CUENTA AND EXISTS (SELECT 1 FROM MED_COMPONENTE WHERE ID_UBICACION = NS.ID_SERVICIO" + 
				    " AND TYPE_UBICACION ='com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico') ";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int total = rs.getInt("reg");
				System.out.println(total);
				objInt.setResult(true);
				objInt.setCampo(total);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error de sintaxis: " + ex.getMessage());
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoStr bfnCambioDeMedidor(entConexion conexOrc, entConexion conexPg, entArchivo entCuenta, Integer iduser, Connection pConORA, Connection pConPG) throws SQLException {
		
		String url="";
		String user="";
		String pass="";
		
		String urlpg="";
		String userpg="";
		String passpg="";
		
		long lSE_IDEstado;
		Integer idSE_IDServicioRet=0;
		Integer idMedMarca =0;
		Long idModelo;
		//Integer idMedModeloInst=0;
		//Integer idMedModeloInst=0;
		entResultadoInt objInt = new entResultadoInt();
		entServElectrico objEle = new entServElectrico();
		entResultadoStr objStr = new entResultadoStr();
		entResultadoStr objStrLog = new entResultadoStr();
		entResultadoLong objLng = new entResultadoLong();
		Long idCuenta;
		String idEstado = "";
		String contratista= "";
		String gCodEjecutor="";
		//String gCodMarMedRet= "";
		String gFechaLectura= "";
		//String gCodModMedRet = "";
		//String gNroMedRet ="";
		//String gNroMedInst="";
		Integer piContReti=0;
		Integer piContInst=0;
		String gCodPropMedInst="";
		Integer gNroSello1 = 0;
		Integer gNroSello2 = 0;
		String gcolSello1 = "";
		String gcolSello2 = "";
		String gubiSello1 = "";
		String gubiSello2 = "";
		//String gCodMarMedInst ="";
		//String gCodModMedInst = "";
		Integer idempresa;
		String cFechaEjecuVta = "";
		// R.I. REQSCOM10 26/07/2023 INICIO
		//String gFactorMedInst = "";
		// R.I. REQSCOM10 26/07/2023 FIN
		try {
			
			url = conexOrc.getUrl();
			user = conexOrc.getUser();
			pass = conexOrc.getPassword();
			
			urlpg = conexPg.getUrl();
			userpg = conexPg.getUser();
			passpg = conexPg.getPassword();
			
			//Cargando los valores de la cuenta
			idCuenta = Long.parseLong(entCuenta.getNroCuenta());
			idempresa = entCuenta.getIdempresa();
			contratista = entCuenta.getCodContratista();
			//gCodModMedRet = entCuenta.getModeloRetiro();
			//gCodMarMedRet = entCuenta.getMarcaRetiro();
			gFechaLectura = entCuenta.getFecLectura();
			//gNroMedRet = entCuenta.getNroMedidorRet();
			gCodPropMedInst = entCuenta.getCodPropiedad();
			gNroSello1 = Integer.parseInt(entCuenta.getNroSello1());
			gcolSello1 = entCuenta.getColorSello1();
			gubiSello1 = entCuenta.getUbicacionSello1();
			gNroSello2 = Integer.parseInt(entCuenta.getNroSello2());
			gcolSello2 = entCuenta.getColorSello2();
			gubiSello2 = entCuenta.getUbicacionSello2();
			//gNroMedInst = entCuenta.getNroMedidorInstalar();
			//gCodModMedInst = entCuenta.getModeloInstalar();
			//gCodMarMedInst = entCuenta.getMarcaInstalar();
			// R.I. REQSCOM10 26/07/2023 INICIO
			//gFactorMedInst = entCuenta.getFactorInstalar();
			// R.I. REQSCOM10 26/07/2023 FIN
			
			objStrLog.setResult(true);
			
			objInt = bfnExisteCuenta(url, user, pass, idempresa, idCuenta);
			if (!objInt.isResult()) {
				objStrLog.setResult(false);
				objStrLog.setCampo("No se encontró registrada la cuenta: "+String.valueOf(idCuenta));
				return objStrLog;	
			}
			
			objInt = new entResultadoInt();
			objInt = bfnCuentaEnFacturacion(url, user, pass, idCuenta);
			//if (!objInt.isResult()) { 
			if(objInt.getCampo()>=1) {  /* ANL */
				objStrLog.setResult(false);
				objStrLog.setCampo("La cuenta: " + idCuenta + " se encuentra en proceso de facturación.");
				return objStrLog;
			}
			objEle = new entServElectrico();
			objEle = bfnTieneServicioElectrico(url, user, pass, idCuenta);
			if (objEle == null) {
				objStrLog.setResult(false);
				objStrLog.setCampo("Se encontró servicio eléctrico Retirado para la cuenta: "+String.valueOf(idCuenta));
				return objStrLog;			
			} else {
				idEstado = objEle.getIdestado();
				idSE_IDServicioRet = objEle.getIdservicio();
			}
			
			if(idEstado=="5") {
				objInt = bfnSE_VerificaVenta(url, user, pass, idCuenta);
				if(!objInt.isResult())
				{	
					//System.out.println("El servicio tiene una Venta por Modificación y se encuentra en un estado que aún no permite Cambio de Medidor para la cuenta: "+String.valueOf(idCuenta));
					objStrLog.setResult(false);
					objStrLog.setCampo("El servicio tiene una Venta por Modificación y se encuentra en un estado que aún no permite Cambio de Medidor para la cuenta: "+String.valueOf(idCuenta));
					return objStrLog;
				}
			}
			objInt = new entResultadoInt();
			objInt = bfnExisteContratista(urlpg, userpg, passpg, idempresa, contratista);
			if (!objInt.isResult()) {
				objStrLog.setResult(false);
				objStrLog.setCampo("No se encontró registrado/activo el contratista: " + contratista);
				return objStrLog;			
			}
			
			if (gCodEjecutor.length()!=0) {
				objInt = new entResultadoInt();
				objInt = bfnExisteEjecutor(urlpg, userpg, passpg, idempresa, gCodEjecutor);
				if(!objInt.isResult())
				{	
					//sprintf(pMnsjErr, "No se encontró registrado/activo el código de ejecutor [%s]", pstLin->gCodEjecutor);
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/activo el código de ejecutor: " + gCodEjecutor);
					return objStrLog;
				}
			}
			
			if(idEstado=="5") {
				objStr = new entResultadoStr();
				objStr = bfnObtenerFechaEjecucionVta(url, user, pass, idCuenta);
				if(!objStr.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró la fecha de ejecución del servicio de ventas para la cuenta: " + idCuenta);
					return objStrLog;
				} //else {}
					
				cFechaEjecuVta = objStr.getCampo();
				if(gFechaLectura != cFechaEjecuVta) { /* ANL */
					objStrLog.setResult(false);
					objStrLog.setCampo("El Servicio se encuentra en Modificación, la Fecha de Lectura [" + gFechaLectura + "] debe ser igual a la Fecha de Ejecución del servicio de venta [" + cFechaEjecuVta + "]. Cuenta:" + idCuenta);
					return objStrLog;
				}
			} //endif estado
			else {  /* ANL */
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");       /* ANL yyyy/MM/dd */
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); /* ANL yyyy-MM-dd */
				LocalDate fecha1 = LocalDate.parse(gFechaLectura, formatter);
				LocalDate fecha2 = LocalDate.now(); //LocalDate.parse(gFechaVta, formatter);
				if(!fecha1.isBefore(fecha2)){
					objStrLog.setResult(false);
					objStrLog.setCampo("La Fecha de Lectura "+ gFechaLectura + " debe ser menor o igual a la Fecha actual " + dtf.format(LocalDateTime.now()));
					return objStrLog;
				}				
			} /* ANL */
			
			objInt = new entResultadoInt();
			objInt = bfnExisteCodigoPropiedadMedidor(urlpg, userpg, passpg, gCodPropMedInst);
			if(!objInt.isResult())
			{	
				objStrLog.setResult(false);
				objStrLog.setCampo("No se encontró registrado/activo el siguiente código de propiedad de medidor a instalar " + gCodPropMedInst);
				return objStrLog;
			} 
			
			if (gNroSello1 > 0) {
				objInt = new entResultadoInt();
				objInt = bfnExisteNroSello(url, user, pass, gNroSello1);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/disponible el siguiente Número de Sello 1 " + gNroSello1);
					return objStrLog;
				} 
				
				objInt = new entResultadoInt();
				objInt = bfnExisteColorSello(url, user, pass, gcolSello1);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/activo el siguiente Color de Sello 1 " + gcolSello1);
					return objStrLog;
				} 
				
				objInt = new entResultadoInt();
				objInt = bfnExisteUbicacionSello(url, user, pass, gubiSello1);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/activa la siguiente Ubicación del Sello 1 " + gubiSello1);
					return objStrLog;
				} 
			}
			
			if (gNroSello2 > 0) {
				objInt = new entResultadoInt();
				objInt = bfnExisteNroSello(url, user, pass, gNroSello2);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/disponible el siguiente Número de Sello 2 " + gNroSello2);
					return objStrLog;
				} 
				
				objInt = new entResultadoInt();
				objInt = bfnExisteColorSello(url, user, pass, gcolSello2);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/activo el siguiente Color de Sello 2 " + gcolSello2);
					return objStrLog;
				} 
				
				objInt = new entResultadoInt();
				objInt = bfnExisteUbicacionSello(url, user, pass, gubiSello2);
				if(!objInt.isResult())
				{	
					objStrLog.setResult(false);
					objStrLog.setCampo("No se encontró registrado/activa la siguiente Ubicación del Sello 2 " + gubiSello2);
					return objStrLog;
				} 
			}
			
			// R.I. REQSCOM10 05/09/2023 INICIO
			List<EntMedidorRetInst> medidores = entCuenta.getMedidores();
			String sqlFactoresValidos = "SELECT count(mmm.id_componente) as nroFactoresValidos\r\n"
					+ "FROM med_medida_medidor mmm, med_medida_modelo AS mmm1\r\n"
					+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo\r\n"
					+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id\r\n"
					+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
					+ " where mc.id_modelo = mmod.id \r\n"
					+ "  and mmod.id_marca = mmar.id \r\n"
					+ " and mc.nro_componente = ? \r\n"
					+ "\r\n"
					+ " and mmod.cod_modelo = ?\r\n"
					+ " and mmar.cod_marca = ?)\r\n"
					+ "    AND mmm1.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
					+ "         where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
					+ "    AND mf.cod_factor = ?\r\n"
					+ "    AND mmm.id_medida = mmm1.id_medida -- Actualiza por cada medida\r\n"
					+ "    AND NOT EXISTS ( -- Verifica que no exista un registro en med_medida_medidor con el nuevo factor no válido\r\n"
					+ "        SELECT 1\r\n"
					+ "        FROM med_medida_medidor AS mm\r\n"
					+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
					+ "where mc.id_modelo = mmod.id\r\n"
					+ "and mmod.id_marca = mmar.id\r\n"
					+ " and mc.nro_componente = ? \r\n"
					+ "and mmod.cod_modelo = ?\r\n"
					+ "and mmar.cod_marca = ?)\r\n"
					+ "        AND NOT EXISTS (\r\n"
					+ "            SELECT 1\r\n"
					+ "            FROM med_medida_modelo AS mmm2\r\n"
					+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo\r\n"
					+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id\r\n"
					+ "            WHERE mmm2.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
					+ "          where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
					+ "            AND mf2.cod_factor = ?\r\n"
					+ "            AND mm.id_medida = mmm2.id_medida\r\n"
					+ "        )\r\n"
					+ "    )";
			
			if (!entCuenta.getFactorInstalar().equalsIgnoreCase("NO")) {
				boolean actualizarFactor = true;
				try (PreparedStatement ps = pConPG.prepareStatement(sqlFactoresValidos)) {
				    // Iniciar la transacción
				    int nroFactoresValidos = 0;
				    
				    // Validando si el factor es válido para cada medidor a instalar
				    for (EntMedidorRetInst medidorRetInst : medidores) {
				    	
				    	if (!medidorRetInst.getMarcaInstalar().equals("0")) {
				    		System.out.println("Validando factores...");
				    		ps.setString(1, medidorRetInst.getNroMedidorInstalar());
					        ps.setString(2, medidorRetInst.getModeloInstalar());
					        ps.setString(3, medidorRetInst.getMarcaInstalar());
					        ps.setString(4, medidorRetInst.getModeloInstalar());
					        ps.setString(5, medidorRetInst.getMarcaInstalar());
					        ps.setString(6, entCuenta.getFactorInstalar());
					        ps.setString(7, medidorRetInst.getNroMedidorInstalar());
					        ps.setString(8, medidorRetInst.getModeloInstalar());
					        ps.setString(9, medidorRetInst.getMarcaInstalar());
					        ps.setString(10, medidorRetInst.getModeloInstalar());
					        ps.setString(11, medidorRetInst.getMarcaInstalar());
					        ps.setString(12, entCuenta.getFactorInstalar());
					        
					        ResultSet rs = ps.executeQuery();
					        while (rs.next()) {
					            nroFactoresValidos = rs.getInt("nroFactoresValidos");
					            System.out.println("medidorRetInst.getNroMedidorInstalar(): " + medidorRetInst.getNroMedidorInstalar());
					            System.out.println("medidorRetInst.getMarcaInstalar(): " + medidorRetInst.getMarcaInstalar());
					            System.out.println("medidorRetInst.getModeloInstalar(): " + medidorRetInst.getModeloInstalar());
					            System.out.println("nroFactoresValidos: " + nroFactoresValidos);
					        }
					        
					        if (nroFactoresValidos == 0) {
					            System.out.println("No se actualizará el factor");
					            actualizarFactor = false;
					            break;
					        }
				    	}
				    }
				} catch (Exception e) {
					System.out.println("Error al validar el factor " + e.getMessage());
					actualizarFactor = false;
				}
				
				String sqlUpdateFactor =
				        "UPDATE med_medida_medidor AS mmm\r\n"
				        + "SET id_factor = mfm.id_factor,\r\n"
				        + "    val_factor = mf.val_factor\r\n"
				        + "FROM med_fac_med_mod AS mfm\r\n"
				        + "JOIN med_factor AS mf ON mfm.id_factor = mf.id\r\n"
				        + "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
				        + "where mc.id_modelo = mmod.id\r\n"
				        + "and mmod.id_marca = mmar.id\r\n"
				        + " and mc.nro_componente = ? \r\n"
				        + "and mmod.cod_modelo = ?\r\n"
				        + "and mmar.cod_marca = ?)\r\n"
				        + "    AND mf.cod_factor = ?";

				try {
				    if (actualizarFactor) {
				        PreparedStatement preparedStatement = pConPG.prepareStatement(sqlUpdateFactor);

				        for (EntMedidorRetInst medidorRetInst : medidores) {
				            // Establecer los parámetros en lugar de concatenar valores directamente
				        	if (!medidorRetInst.getMarcaInstalar().equals("0")) {
				        		System.out.println("Actualizando factores...");
				        		preparedStatement.setString(1, medidorRetInst.getNroMedidorInstalar());
					            preparedStatement.setString(2, medidorRetInst.getModeloInstalar());
					            preparedStatement.setString(3, medidorRetInst.getMarcaInstalar());
					            preparedStatement.setString(4, entCuenta.getFactorInstalar());

					            try {
					                System.out.println("sqlUpdateFactor: " + sqlUpdateFactor);
					                preparedStatement.executeUpdate();
					            } catch (SQLException e) {
					                // Si hay un error, hacer un rollback
					                System.out.println("Error al actualizar, ejecutando rollback...");
					                //pConPG.rollback();
					                throw e; // Lanzar la excepción nuevamente para manejarla en un nivel superior si es necesario
					            }
				        	}
				        }
				        // Hacer commit solo si todas las actualizaciones fueron exitosas
				        System.out.println("Se actualizaron todos los factores, ejecutando el commit...");
				        pConPG.commit();
				    }
				} catch (Exception e) {
				    System.out.println("Error al actualizar el factor " + e.getMessage());
				    pConPG.rollback();
				}
				// R.I. REQSCOM10 05/09/2023 FIN
			}
			
			System.out.println("Numero de pares de medidores en el plano: " + medidores.size());
			
			for (EntMedidorRetInst medidorRetInst : medidores) {
				System.out.println("Procesando: " + medidorRetInst.toString());
				if (!medidorRetInst.getMarcaRetiro().equals("0")) {
					System.out.println("Existe medidor retiro");
					objInt = new entResultadoInt();
					objInt = bfnExisteMarcaMedidor(urlpg, userpg, passpg, medidorRetInst.getMarcaRetiro());
					if(!objInt.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de marca de medidor a retirar: " + medidorRetInst.getMarcaRetiro());
						return objStrLog;
					} else {
						idMedMarca = objInt.getCampo();
					}
					
					Long idMedModeloRet = null;
					objLng = new entResultadoLong();
					objLng = bfnExisteModeloMedidor(urlpg, userpg, passpg, idMedMarca, medidorRetInst.getModeloRetiro());
					if(!objLng.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de modelo de medidor a retirar: " + medidorRetInst.getMarcaRetiro() + ", ID marca medidor "+String.valueOf(idMedMarca));
						return objStrLog;
					} else {
						idMedModeloRet = objLng.getCampo();
					}
					
					objInt = new entResultadoInt();
					objInt = bfnExisteNumeroMedidor(urlpg, userpg, passpg, medidorRetInst.getNroMedidorRet(), idMedModeloRet, idempresa, idSE_IDServicioRet, "1");
					if(!objInt.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de modelo de medidor a retirar: " + medidorRetInst.getNroMedidorRet() + ", ID modelo de medidor: "
								+ String.valueOf(idMedModeloRet) + ", ID servicio: " + idSE_IDServicioRet);
						return objStrLog;
					}
					
					piContReti = medidorRetInst.getLstLectRet().size();			
					for(int i = 0; i < piContReti; i++) {
						objInt = new entResultadoInt();
						String codigo = "";
						codigo = medidorRetInst.getLstLectRet().get(i).getCodigo();
						objInt = bfnExisteCodigoLectura(urlpg, userpg, passpg, medidorRetInst.getLstLectRet().get(i).getCodigo());
						if(!objInt.isResult())
						{	
							objStrLog.setResult(false);
							objStrLog.setCampo("No se encontró registrado el siguiente código de lectura del medidor a retirar: " + codigo);
							return objStrLog;
						}
					}
						
					entLecturaMed entLectura = new entLecturaMed();
					entLectura = bfnCorrespondenciaLecturasMedidor(urlpg, userpg, passpg, medidorRetInst.getNroMedidorRet(), idMedModeloRet, idSE_IDServicioRet, "1", medidorRetInst.getLstLectIns());
					if(!entLectura.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("Existen diferencias entre las lecturas del medidor y las lecturas del archivo de entrada para el siguiente número de medidor a instalar " + medidorRetInst.getMarcaRetiro() + ", ID modelo de medidor "
								+ String.valueOf(idMedModeloRet) + ", ID servicio" + idSE_IDServicioRet);
						return objStrLog;
					}
					boolean retorno = bfnRetirarMedidor(pConORA, pConPG, idempresa, iduser, medidorRetInst.getNroMedidorRet(), piContReti,
							idMedModeloRet, idSE_IDServicioRet, idCuenta, contratista, gNroSello1, gNroSello2, gcolSello1, 
							gcolSello2, gubiSello1, gubiSello2, medidorRetInst.getLstLectRet(), entCuenta.getFecLectura());
					if (!retorno) {
						objStrLog.setResult(false);
						objStrLog.setCampo("No se pudo retirar el siguiente medidor: " + medidorRetInst.getNroMedidorRet());
						return objStrLog;
					}
				} else {
					System.out.println("No existe medidor retiro");
				}
				
				if (!medidorRetInst.getMarcaInstalar().equals("0")) {
					System.out.println("Existe medidor a instalar");
					objInt = new entResultadoInt();
					objInt = bfnExisteMarcaMedidor(urlpg, userpg, passpg, medidorRetInst.getMarcaInstalar());
					if(!objInt.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de marca de medidor a instalar: " + medidorRetInst.getMarcaInstalar());
						return objStrLog;
					} else {
						idMedMarca = objInt.getCampo();
					}
					
					Long idMedModeloInst = null;
					objLng = new entResultadoLong();
					objLng = bfnExisteModeloMedidor(urlpg, userpg, passpg, idMedMarca, medidorRetInst.getModeloInstalar());
					if(!objLng.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de modelo de medidor a instalar: " + medidorRetInst.getModeloInstalar() + ", ID marca medidor "+String.valueOf(idMedMarca));
						return objStrLog;
					} else {
						idMedModeloInst = objLng.getCampo();
					}
					
					piContInst = medidorRetInst.getLstLectIns().size();			
					for(int i = 0; i < piContInst; i++) {
						objInt = new entResultadoInt();
						String codigo = "";
						codigo = medidorRetInst.getLstLectIns().get(i).getCodigo();
						objInt = bfnExisteCodigoLectura(urlpg, userpg, passpg, medidorRetInst.getLstLectIns().get(i).getCodigo());
						if(!objInt.isResult())
						{	
							objStrLog.setResult(false);
							objStrLog.setCampo("No se encontró registrado el siguiente código de lectura del medidor a instalar: " + codigo);
							return objStrLog;
						}
					}
					
					entLecturaMed entLecturaInst = new entLecturaMed();
					entLecturaInst = bfnCorrespondenciaLecturasMedidor(urlpg, userpg, passpg, medidorRetInst.getNroMedidorInstalar(), idMedModeloInst, idSE_IDServicioRet, "2", medidorRetInst.getLstLectIns());
					if(!entLecturaInst.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("Existen diferencias entre las lecturas del medidor y las lecturas del archivo de entrada para el siguiente número de medidor a instalar " + medidorRetInst.getMarcaInstalar() + ", ID modelo de medidor "
								+ String.valueOf(idMedModeloInst) + ", ID servicio" + idSE_IDServicioRet);
						return objStrLog;
					}
					
					
					objInt = new entResultadoInt();
					System.out.println(medidorRetInst.getNroMedidorInstalar() + " " + idMedModeloInst);
					objInt = bfnExisteNumeroMedidor(urlpg, userpg, passpg, medidorRetInst.getNroMedidorInstalar(), idMedModeloInst, idempresa, 0, "2");
					if(!objInt.isResult())
					{	
						objStrLog.setResult(false);
						objStrLog.setCampo("No se encontró registrado/activo el siguiente código de modelo de medidor a instalar: " + medidorRetInst.getNroMedidorInstalar() + ", ID modelo de medidor: "
								+ String.valueOf(medidorRetInst.getNroMedidorInstalar()));
						return objStrLog;
					}
					
					
					
					boolean retornoInt = bfnInstalarMedidor(pConORA, pConPG, idempresa, iduser, medidorRetInst.getNroMedidorInstalar(), piContInst,
							idMedModeloInst, idSE_IDServicioRet, idCuenta, contratista, gNroSello1, gNroSello2, gcolSello1, 
							gcolSello2, gubiSello1, gubiSello2, medidorRetInst.getLstLectIns(), entCuenta.getFecLectura(),
							// R.I. REQSCOM10 27/07/2023 INICIO
							idMedMarca, medidorRetInst.getModeloInstalar(), entCuenta.getCodPropiedad(), entCuenta.getFactorInstalar());
							// R.I. REQSCOM10 27/07/2023 FIN
					if (!retornoInt) {
						objStrLog.setResult(false);
						objStrLog.setCampo("No se pudo instalar el siguiente medidor: " + medidorRetInst.getNroMedidorInstalar() );
						return objStrLog;
					}
				} else {
					System.out.println("No existe medidor a instalar");
				}
				
			}
			
			/* Crear ORM Finalizada - ANL_20230419 */
			entParamORM ParamOrm = CargarParametrosORM(pConPG);
			objStr = new entResultadoStr();
			objStr = bfnGenerarORM(pConPG, idSE_IDServicioRet, iduser, ParamOrm);
			if(!objStr.isResult())
			{	
				objStrLog.setResult(false);
				objStrLog.setCampo(String.format("No se pudo generar ORM genérica. Cuenta [%s] [%s]", entCuenta.getNroCuenta(), objStr.getCampo()));
				return objStrLog;
			}

			
		} catch (Exception e) {
			objStrLog.setCampo("Error: " + e.getMessage());
			objStrLog.setResult(false);
			e.printStackTrace();
		}
		
		return objStrLog;
		
	}
	
	public entResultadoInt bfnCuentaEnFacturacion(String url, String user, String pass, long pCuenta) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select count(1) total FROM synergia.nuc_cuenta nc, synergia.cia_agrup_ciclo ac, synergia.fac_estado_ciclo ec " + 
					"WHERE nc.id_cuenta = ac.id_cuenta AND ac.id_estado_ciclo = ec.id_estado_ciclo " + 
					"AND ac.id_estado_ciclo <> 18 AND nc.nro_cuenta = " + String.valueOf(pCuenta);
			ResultSet rsUser = st.executeQuery(sql);
			while (rsUser.next()) {
				int total = rsUser.getInt("total");
				System.out.println(total);
				objInt.setResult(true);
				objInt.setCampo(total);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {	
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia de la Cuenta " +String.valueOf(pCuenta)+". Revisar Log del Proceso");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entServElectrico bfnTieneServicioElectrico(String url, String user, String pass, long pCuenta) throws SQLException
	{
		entServElectrico obj = null;
		Connection BaseDatos = null;
		Statement st = null;
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT trim(WSE.ID_STATE) ID_STATE, SE.ID_ESTADO, SE.ID_SERVICIO"
					+ "	FROM synergia.NUC_CUENTA NC, synergia.NUC_SERVICIO NS, synergia.SRV_ELECTRICO SE, synergia.WKF_WORKFLOW WSE"
					+ " WHERE NC.ID_CUENTA = NS.ID_CUENTA"
					+ "	AND NS.ID_SERVICIO = SE.ID_SERVICIO"
					+ "	AND NS.TIPO = 'ELECTRICO'"
					+ "	AND SE.ID_WORKFLOW = WSE.ID_WORKFLOW "
					+ "	AND NC.NRO_CUENTA = " + String.valueOf(pCuenta) 
					+ " AND WSE.ID_STATE not in ('Retirado', 'SujetoVerificacionRetirado')";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				obj = new entServElectrico();
				obj.setIdstate(rs.getString("ID_STATE"));
				obj.setIdestado(rs.getString("ID_ESTADO"));
				obj.setIdservicio(rs.getInt("ID_SERVICIO"));
				System.out.println("ID_STATE: " + rs.getString("ID_STATE"));
				System.out.println("ID_ESTADO: " + rs.getString("ID_ESTADO"));
				System.out.println("ID_SERVICIO: " + rs.getString("ID_SERVICIO"));
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {	
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar servicio eléctrico de la Cuenta " + String.valueOf(pCuenta)  + ". Revisar Log del Proceso.");
			obj = null;
		}
		return obj;
	}
	
	public entResultadoInt bfnExisteContratista(String url, String user, String pass, int pidEmpresa, String contratista) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT COUNT(1) total FROM schscom.com_contratista WHERE cod_contratista = '" + contratista + "'";
			ResultSet rsUser = st.executeQuery(sql);
			while (rsUser.next()) {
				int total = rsUser.getInt("total");
				System.out.println(total);
				objInt.setResult(true);
				objInt.setCampo(total);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {	
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del contratista " +contratista+". Revisar Log del Proceso");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteEjecutor(String url, String user, String pass, int pidEmpresa, String ejecutor) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT COUNT(1) total FROM schscom.com_contratista con, com_ejecutor eje "
					+ " WHERE eje.cod_ejecutor = '" + ejecutor +"'" /* ANL */
					+ " AND eje.activo = 'S' " 
					+ " AND con.id_contratista = eje.id_contratista";
			ResultSet rsUser = st.executeQuery(sql);
			while (rsUser.next()) {
				int total = rsUser.getInt("total");
				System.out.println(total);
				objInt.setResult(true);
				objInt.setCampo(total);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {	
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del código de ejecutor " +ejecutor+". Revisar Log del Proceso");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteMarcaMedidor(String url, String user, String pass, String codMarca) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			//sql = "SELECT id id_marca FROM schscom.med_marca WHERE cod_marca = '" + codMarca + "' AND activo = 'S' "; // COMENTADO RSOSA INC000110391141 30062023	
			sql = "SELECT id id_marca FROM schscom.med_marca WHERE cod_marca = '" + codMarca + "' AND activo = 'S' AND id_tip_componente = 1 "; // COMENTADO RSOSA INC000110391141 30062023	
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id_marca");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia de la marca del medidor " + codMarca);
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoLong bfnExisteModeloMedidor(String url, String user, String pass, Integer idMarca,String codModelo) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoLong objInt = new entResultadoLong();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT id id_modelo FROM schscom.med_modelo WHERE id_marca = " + String.valueOf(idMarca) + " AND cod_modelo = '" + codModelo + "' AND activo = 'S' ";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Long id = rs.getLong("id_modelo");
				System.out.println("id_modelo:"+ id);
				System.out.println("cod_modelo:"+ codModelo);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del modelo del medidor " + codModelo);
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteNumeroMedidor(String url, String user, String pass, String nroMedidor, Long idMedModelo, int pidEmpresa,
			Integer idservicio, String opcion) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0;
		System.out.println("idservicio: " + String.valueOf(idservicio));
		System.out.println("nroMedidor: " +nroMedidor);
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM schscom.med_componente c, schscom.med_est_componente e " 
				+ " WHERE c.nro_componente = '" + nroMedidor + "' "
				+ " AND c.id_modelo = " + idMedModelo
				+ " AND c.id_est_componente = e.id ";
			
			if (opcion == "1") {
				sql += " AND c.id_ubicacion =" + String.valueOf(idservicio)
					+ " AND e.cod_interno = 'Instalado'";
			} else {
				sql += " AND e.cod_interno = 'Disponible'";
			}
			
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println("total NumeroMedidor:" + id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}
			
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del modelo del medidor ");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteCodigoLectura(String url, String user, String pass, String codigo) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0;
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM schscom.med_medida WHERE cod_medida = '" + codigo +  "' " ;
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println("total ExisteCodigoLectura:" + id);
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}
			
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del Código de Lectura " + codigo);
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteCodigoPropiedadMedidor(String url, String user, String pass, String codPropMed) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0;
		System.out.println("codPropMed: "+codPropMed);
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM schscom.med_propiedad WHERE cod_propiedad = '" + codPropMed + "' AND activo = 'S' " ;
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}

			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar existencia del Código de Propiedad de Medidor " + codPropMed);
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteNroSello(String url, String user, String pass, Integer idsello) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0; /* ANL */
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM synergia.sel_sello sel, synergia.sel_bolsa_sellos bol WHERE sel.id_motivo_estado = 1 "
			+ " AND sel.id_bolsa_sellos  = bol.id_bolsa_sellos AND sel.nro_sello = " + String.valueOf(idsello);
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}
			
			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar el número de sello " + String.valueOf(idsello) + " . Revisar Log del Proceso.");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteColorSello(String url, String user, String pass, String colorSello) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0;
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM synergia.sel_color  WHERE activo = 'S' "
			+ " AND cod_color = '" + colorSello + "'";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}

			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar el Color de Sello " + colorSello + " . Revisar Log del Proceso.");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnExisteUbicacionSello(String url, String user, String pass, String ubiSello) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		int id = 0;
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "SELECT count(1) total FROM synergia.sel_ubicacion WHERE cod_ubicacion = '" + ubiSello + "' And activo = 'S' ";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("total");
				System.out.println(id);
				objInt.setResult(true);
				objInt.setCampo(id);
			}
			
			if (id==0) { /* ANL */
				objInt.setResult(false);
				objInt.setCampo(null);
			}

			st.close();
			BaseDatos.close();
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar la ubicación del Sello " + ubiSello + " . Revisar Log del Proceso.");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		return objInt;
	}
	
	public entResultadoInt bfnSE_VerificaVenta(String url, String user, String pass, Long pCuenta) throws SQLException
	{
		Connection BaseDatos = null;
		Statement st = null;
		entResultadoInt objInt = new entResultadoInt();
		String sql = "";
		BaseDatos = DriverManager.getConnection(url, user, pass);
		st = BaseDatos.createStatement();
		sql = "SELECT COUNT(1) total " + 
			"FROM SYNERGIA.NUC_CUENTA NC, SYNERGIA.NUC_SERVICIO NS, SYNERGIA.SRV_ELECTRICO SE, SYNERGIA.PRD_PRODUCTO PRD, SYNERGIA.VTA_SOL_SRV_ELE SELE, SYNERGIA.VTA_SRV_VENTA SVEN, SYNERGIA.ORD_ORDEN OVEN, SYNERGIA.WKF_WORKFLOW WORD " +
			"WHERE NC.ID_CUENTA = NS.ID_CUENTA" + 
			"AND NS.ID_SERVICIO = SE.ID_SERVICIO" + 
			"AND NS.TIPO = 'ELECTRICO' " + 
			"AND SE.ID_SERVICIO = SELE.ID_SRV_ELECTRICO " + 
			"AND SELE.ID_SOL_SRV_ELE = SVEN.ID_SOL_SRV_ELE " + 
			"AND SVEN.ID_ORD_VENTA = OVEN.ID_ORDEN " + 
		    "AND SVEN.ID_PRODUCTO = PRD.ID_PRODUCTO " + 
		    "AND PRD.TIP_PRODUCTO = 'MOD' " + 
			"AND OVEN.ID_WORKFLOW = WORD.ID_WORKFLOW " + 
			"AND WORD.ID_STATE NOT IN ('Anulada', 'Finalizada', 'RevisionFinal') " + 
			"AND NC.NRO_CUENTA = " + String.valueOf(pCuenta);
		try {
			int count = 0;
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				count = rs.getInt("total");
				System.out.println(count);
				objInt.setResult(true);
				objInt.setCampo(count);
			}
			st.close();
			BaseDatos.close();
			if (count == 0) {
				objInt.setResult(true);
				objInt.setCampo(0);			
			} else {
				BaseDatos = DriverManager.getConnection(url, user, pass);
				st = BaseDatos.createStatement();
				sql = "SELECT COUNT(1) total " + 
					"FROM SYNERGIA.NUC_CUENTA NC, SYNERGIA.NUC_SERVICIO NS, SYNERGIA.SRV_ELECTRICO SE, SYNERGIA.PRD_PRODUCTO PRD, SYNERGIA.VTA_SOL_SRV_ELE SELE, SYNERGIA.VTA_SRV_VENTA SVEN, SYNERGIA.ORD_ORDEN OVEN, SYNERGIA.WKF_WORKFLOW WORD " +
					"WHERE NC.ID_CUENTA = NS.ID_CUENTA" + 
					"AND NS.ID_SERVICIO = SE.ID_SERVICIO" + 
					"AND NS.TIPO = 'ELECTRICO' " + 
					"AND SE.ID_SERVICIO = SELE.ID_SRV_ELECTRICO " + 
					"AND SELE.ID_SOL_SRV_ELE = SVEN.ID_SOL_SRV_ELE " + 
					"AND SVEN.ID_ORD_VENTA = OVEN.ID_ORDEN " + 
				    "AND SVEN.ID_PRODUCTO = PRD.ID_PRODUCTO " + 
				    "AND PRD.TIP_PRODUCTO = 'MOD' " + 
				    "AND SVEN.ESTADO='Ejecutado' " + 
					"AND OVEN.ID_WORKFLOW = WORD.ID_WORKFLOW " + 
					"AND WORD.ID_STATE NOT IN ('Anulada', 'Finalizada', 'RevisionFinal') " + 
					"AND NC.NRO_CUENTA = " + String.valueOf(pCuenta);
				try {
					int count2 = 0;
					ResultSet rs2 = st.executeQuery(sql);
					while (rs2.next()) {
						count2 = rs2.getInt("total");
						objInt.setResult(true);
						objInt.setCampo(count2);
					}
					st.close();
					BaseDatos.close();
				} catch (SQLException ex) {
					st.close();
					BaseDatos.close();
					System.err.println("Error al verificar estado (Ejecutado) de venta de la Cuenta " + String.valueOf(pCuenta) + " . Revisar Log del Proceso.");
					objInt.setResult(false);
					objInt.setCampo(null);
				}
			}
			
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al verificar estado de venta de la Cuenta " + String.valueOf(pCuenta) + " . Revisar Log del Proceso.");
			objInt.setResult(false);
			objInt.setCampo(null);
		}
		
		return objInt;
	}
	
	public entResultadoStr bfnObtenerFechaEjecucionVta(String url, String user, String pass, Long pCuenta) throws SQLException {
		Connection BaseDatos = null;
		Statement st = null;
		String valor = "";
		entResultadoStr objStr = new entResultadoStr();
		String sql = "";
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			sql = "Select to_char(sven.fecha_ejecucion, 'DD/MM/YYYY') fecha " +
				  " FROM synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se, synergia.vta_sol_srv_ele sele, synergia.vta_srv_venta sven " +
				  " WHERE nc.id_cuenta = ns.id_cuenta " +
				  "  AND ns.id_servicio = se.id_servicio " +
				  "  AND ns.tipo = 'ELECTRICO' " +
				  "  AND se.id_servicio = sele.id_srv_electrico " +
				  "  AND sele.id_sol_srv_ele = sven.id_sol_srv_ele " +
				  "  AND nc.nro_cuenta = " + String.valueOf(pCuenta)
				+ "  AND sven.estado = 'Ejecutado' "; /* ANL */
			try {
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()) {
					valor = rs.getString("fecha");
					System.out.println(valor);
					objStr.setResult(true);
					objStr.setCampo(valor);
				}
				st.close();
				BaseDatos.close();
			} catch (SQLException ex) {
				st.close();
				BaseDatos.close();
				System.err.println("La consulta no arrojo resultado para la cuenta : " + String.valueOf(pCuenta));
				objStr.setResult(true);
				objStr.setCampo(null);
			}
			
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error al obtener fecha de ejecución del servicio de ventas para la cuenta : " + String.valueOf(pCuenta));
			objStr.setResult(false);
			objStr.setCampo(null);
		}
		return objStr;
	}
	
	public entLecturaMed bfnCorrespondenciaLecturasMedidor(String url, String user, String pass, String nroMedidor, long idMedModelo, Integer idServicio, 
			String pOpc,  List<entLectura> pArrLect) throws SQLException
	{
		
		Connection BaseDatos = null;
		Statement st = null;
		entLecturaMed obj = new entLecturaMed();
		String sql = "";
		
		try {
			BaseDatos = DriverManager.getConnection(url, user, pass);
			st = BaseDatos.createStatement();
			if (pOpc == "1") { // 1:Retirar
				sql = "SELECT id id_componente FROM schscom.med_componente WHERE nro_componente = '" + nroMedidor + "' "
						+ " AND id_modelo = " + String.valueOf(idMedModelo)
						+ " AND id_ubicacion = " + String.valueOf(idServicio);
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()) {
					Long id = rs.getLong("id_componente");
					System.out.println("id_componentev1: " + id);
					obj.setId_componente(id);
				}
				
			} else {
				sql = "SELECT id id_componente FROM schscom.med_componente WHERE nro_componente = '" + nroMedidor + "' " 
						+ " AND id_modelo = " + String.valueOf(idMedModelo);
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()) {
					Long id = rs.getLong("id_componente");
					System.out.println("id_componentev2: " + id);
					obj.setId_componente(id);
				}
				
			}
			
			sql = "SELECT me.cod_medida" 
				+ " FROM schscom.med_medida me, schscom.med_medida_medidor med"
				+ " WHERE med.id_componente = " + String.valueOf(obj.getId_componente())
				+ " AND me.id = med.id_medida ";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				String codigo = rs.getString("cod_medida");
				System.out.println("cod_medida: " + codigo);
				obj.setCod_medida(codigo);
			}
			obj.setResult(true);
			
			st.close();
			BaseDatos.close();
			
		} catch (SQLException ex) {
			st.close();
			BaseDatos.close();
			System.err.println("Error en el metodo bfnCorrespondenciaLecturasMedidor. Revisar Log del Proceso.");
			obj.setResult(false);
		}
		return obj;
	}
	
	public boolean bfnRetirarMedidor(Connection pConORA, Connection pConPG, Integer pIDEmpr,
            Integer pIdUser,
            String nroMedRet,
            Integer piContReti,
            Long pidMedModelo,
            Integer pIDServicio,
            Long pnroCuenta,
            String gCodContratista,
            Integer gNroSello1,
			Integer gNroSello2,
			String gcolSello1,
			String gcolSello2,
			String gubiSello1,
			String gubiSello2,
			List<entLectura> arrLecturas,
			String pFechaLectura 
			) throws SQLException
	{
//		Connection BaseDatos = null;
		Statement st = null;
		Connection BaseDatosOrc = null;
		Statement stOrc = null;
		String sql = "";
		Integer idempresa=0;
		Integer IdMotivoEstado=0;
		Integer dynamic = 0;
		Integer idpropiedad = 0;
		Long idcomponente = 12345678910L;
		Integer vIdTipMagnitud = 0;
		Integer vIdEstMagnitud = 0;
//		String urlpg=""; String userpg=""; String passpg="";
//		String urlOrc=""; String userOrc=""; String passOrc="";
		
//		urlOrc = conexOrc.getUrl();
//		userOrc = conexOrc.getUser();
//		passOrc = conexOrc.getPassword();
//		
//		urlpg = conexPg.getUrl();
//		userpg = conexPg.getUser();
//		passpg = conexPg.getPassword();
		
		try {
			
			sql = "SELECT ne.id_empresa FROM schscom.nuc_empresa ne  WHERE ne.cod_partition = 'GLOB'";
//			BaseDatos = DriverManager.getConnection(urlpg, userpg, passpg);
			st = pConPG.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				idempresa = rs.getInt("id_empresa");
				System.out.println(idempresa);				
			}
			
			sql = "SELECT id_motivo_estado FROM synergia.sel_motivo_estado 	WHERE cod_interno = 'Retirado' AND activo = 'S'";
//			BaseDatosOrc = DriverManager.getConnection(urlOrc, userOrc, passOrc);
			stOrc = pConORA.createStatement();
			ResultSet rsm = stOrc.executeQuery(sql);
			while (rsm.next()) {
				IdMotivoEstado = rsm.getInt("id_motivo_estado");
				System.out.println("IdMotivoEstado: "+IdMotivoEstado);				
			}
			
			dynamic = -1;
			System.out.println("nroMedRet:" + nroMedRet);
			System.out.println("pidMedModelo:" + pidMedModelo);
			System.out.println("id_ubicacion:" + pIDServicio);
			sql = "SELECT coalesce(id_dynamicobject,-1) dynamic, id_propiedad, id id_componente FROM schscom.med_componente "
					+" WHERE nro_componente = '" + nroMedRet + "' AND id_modelo = " + pidMedModelo 
					+" AND id_ubicacion = "+String.valueOf(pIDServicio);
			ResultSet rsa = st.executeQuery(sql);
			while (rsa.next()) {
				dynamic = rsa.getInt("dynamic");
				idpropiedad = rsa.getInt("id_propiedad");
				idcomponente = rsa.getLong("id_componente");
				System.out.println("dynamic:" + dynamic);
				System.out.println("idpropiedad:" + idpropiedad);
				System.out.println("idcomponente:" + idcomponente);
			}
			
			Integer seqdynamic = 0;
			if (dynamic == -1) {
				
				sql = "SELECT nextval('schscom.sqdynamicbusinessobject') ID ";
				ResultSet rsb = st.executeQuery(sql);
				while (rsb.next()) {
					seqdynamic = rsb.getInt("ID");
					System.out.println("seqdynamic: " + seqdynamic);
				}
				sql = "INSERT INTO schscom.DYO_OBJECT(ID) VALUES(" + String.valueOf(seqdynamic) + ")";
				st.execute(sql);
				//seqdynamic = 2304704;
			}
			
			
			Integer idcliente = 0;
			sql = "Select cli.id_cliente from synergia.nuc_cliente cli, synergia.nuc_cuenta nc where cli.id_cliente = nc.id_cliente and nc.nro_cuenta = "+ String.valueOf(pnroCuenta);
			ResultSet rsj = stOrc.executeQuery(sql);
			while (rsj.next()) {
				idcliente = rsj.getInt("id_cliente");
				System.out.println("id_cliente: "+idcliente);
			}

			
			sql = "UPDATE schscom.med_componente ";
			sql += " SET id_est_componente = (select id from schscom.med_est_componente where cod_interno = 'Retirado'), ";
			sql += " id_ubicacion = (case when id_propiedad = 25 then " + String.valueOf(idcliente);
			sql += " else (select id from schscom.com_contratista ";
			sql += " where cod_contratista = '" + String.valueOf(gCodContratista) + "' ) end), ";
			sql += " type_ubicacion = (case when id_propiedad = 25 ";
			sql += " then 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente' ";
			sql += " else 'com.synapsis.synergia.common.domain.Contratista' ";
			sql += " end) ";
			
			if(dynamic == -1) {
				sql += " ,id_dynamicobject = " + String.valueOf(seqdynamic);
			}
			sql += " WHERE nro_componente = '" + nroMedRet + "' ";
			sql += " AND id_modelo = " + pidMedModelo;
			sql += " AND id_ubicacion = " + String.valueOf(pIDServicio);
			//System.out.println(sql);
			st.execute(sql);
			
			Long idhiscomponente = null;
			sql = "SELECT coalesce(max(id_his_componente),0) ID FROM schscom.med_his_componente  WHERE id_componente = "+String.valueOf(idcomponente) + " AND id_est_componente = 18";
			ResultSet rsb = st.executeQuery(sql);
			while (rsb.next()) {
				idhiscomponente = rsb.getLong("ID");
				System.out.println("idhiscomponente : " +idhiscomponente);
			}
			
			sql = "UPDATE schscom.med_his_componente ";
			sql += "SET fec_hasta = current_timestamp ";
			sql += "WHERE id_his_componente = " + String.valueOf(idhiscomponente);
			st.execute(sql);
			
			Long seqauditevent = null;
			sql = "SELECT nextval('schscom.sqauditevent') ID ";
			ResultSet rsk = st.executeQuery(sql);
			while (rsk.next()) {
				seqauditevent = rsk.getLong("ID");
				System.out.println("sqauditevent: "+seqauditevent);
			}
			
			Integer idcliente2 = 0;
			sql = "Select cli.id_cliente from synergia.nuc_cliente cli, synergia.nuc_cuenta nc where cli.id_cliente = nc.id_cliente and nc.nro_cuenta = "+ String.valueOf(pnroCuenta);
			ResultSet rsy = stOrc.executeQuery(sql);
			while (rsy.next()) {
				idcliente2 = rsy.getInt("id_cliente");
				System.out.println(idcliente2);
			}

			//Insertando med_his_componente
			sql = "INSERT INTO schscom.MED_HIS_COMPONENTE(id_his_componente, id_componente, id_est_componente, fec_desde, id_ubicacion, type_ubicacion) " 
				+ " Select " + String.valueOf(seqauditevent) + "," +  String.valueOf(idcomponente) + ","
				+ " (Select id from schscom.med_est_componente where cod_interno = 'Retirado'), current_timestamp, ";
			if (idpropiedad == 25) {
				sql += String.valueOf(idcliente2);
				sql += ", 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente' ";
			} else {
				sql += " (select id from schscom.com_contratista where cod_contratista = '"+String.valueOf(gCodContratista) + "' ) ";
				sql += ", 'com.synapsis.synergia.common.domain.Contratista' ";
			}
			st.execute(sql);
			
			//Insertando auditoria
			sql = "INSERT INTO schscom.FWK_AUDITEVENT(id, usecase, objectref, id_fk, fecha_ejecucion, specific_auditevent, id_user) ";
			sql += "VALUES( "+ String.valueOf(seqauditevent) + ",'Medidor.UPDATE', 'com.synapsis.synergia.med.domain.componente.Medidor', ";
			sql += String.valueOf(idcomponente) + ", current_timestamp, 'COMPONENTE', "+ pIdUser + ") ";
			st.execute(sql);

			sql = "SELECT id_tip_magnitud ID FROM schscom.med_tip_magnitud WHERE cod_interno = 'Retiro' ";
			ResultSet rst = st.executeQuery(sql);
			while (rst.next()) {
				vIdTipMagnitud = rst.getInt("ID");
				System.out.println("id_tip_magnitud:"+vIdTipMagnitud);
			}
			
			sql = "SELECT id_est_magnitud ID FROM schscom.fac_est_magnitud WHERE codigo = '01' ";
			ResultSet rsh = st.executeQuery(sql);
			while (rsh.next()) {
				vIdEstMagnitud = rsh.getInt("ID");
				System.out.println("id_est_magnitud: "+vIdEstMagnitud);
			}
			
			Integer vIdMedida=0;
			String vIdFactor="";
			Integer vIdTipCalculo=0;
			Integer vIdTipo_cons=0;
			Integer vICantEnteros=0;
			Integer vICantDecimales=0;
			Integer vIdSecMagnitud=0;
			Integer xid_servicio=0;
			
			for(int i = 0; i < piContReti; i++) {
				
				String vCodigo = "";
				String vdValor = "";
				vCodigo = arrLecturas.get(i).getCodigo();
				vdValor = arrLecturas.get(i).getValor();
				
				sql = "SELECT med.id_medida, fac.val_factor, med.id_tip_calculo, tc.id_tipo_cons, med.cant_enteros, med.cant_decimales "
						+" FROM schscom.med_medida me, schscom.med_medida_medidor med, schscom.med_factor fac, schscom.med_ent_dec ed, schscom.fac_tipo_consumo tc "
						+" WHERE med.id_componente = "+ String.valueOf(idcomponente) 
						+ " AND me.id = med.id_medida "
						+" AND med.id_factor = fac.id "
						+" AND med.id_ent_dec = ed.id "
						+" AND med.id_medida = tc.id_medida "
						+" AND me.cod_medida = '" + vCodigo + "' ";
				try {
					ResultSet rsp = st.executeQuery(sql);
					vIdFactor = "''";
					while (rsp.next()) {
						vIdMedida = rsp.getInt("id_medida");
						vIdFactor = rsp.getString("val_factor");
						vIdTipCalculo = rsp.getInt("id_tip_calculo");
						vIdTipo_cons = rsp.getInt("id_tipo_cons");
						vICantEnteros = rsp.getInt("cant_enteros");
						vICantDecimales = rsp.getInt("cant_decimales");
						System.out.println("vIdMedida:"+vIdMedida);
						System.out.println("vIdFactor:"+vIdFactor);
						System.out.println("vIdTipCalculo:"+vIdTipCalculo);
					}
					
				} catch (SQLException ex) {
					st.close();
//					BaseDatos.close();
					System.err.println("Error, no se encontró información en 'qRet6'. Cuenta : " + String.valueOf(pnroCuenta) + "Revisar Log del Proceso");
					return false;
				}
				
				sql = "SELECT se.sec_magnitud + 1 magnitud, se.id_servicio "
						+"FROM synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se "
						+"WHERE nc.id_cuenta = ns.id_cuenta "
						+"AND ns.id_servicio = se.id_servicio "
						+"AND ns.tipo = 'ELECTRICO' "
						+"AND nc.nro_cuenta = " + String.valueOf(pnroCuenta) ;
				try {
					ResultSet rsd = stOrc.executeQuery(sql);
					while (rsd.next()) {
						vIdSecMagnitud = rsd.getInt("magnitud");
						xid_servicio = rsd.getInt("id_servicio");
						System.out.println("vIdSecMagnitud:"+vIdSecMagnitud);
						System.out.println("xid_servicio:"+xid_servicio);
					}
					
				} catch (SQLException ex) {
					st.close();
//					BaseDatos.close();
					System.err.println("Error en bfnRetirarMedidor_Into_vIdSecMagnitud. Cuenta : " + String.valueOf(pnroCuenta) + "Revisar Log del Proceso");
					return false;
				}
				
				sql = "UPDATE synergia.srv_electrico "
					+"SET sec_magnitud = " + String.valueOf(vIdSecMagnitud)
					+"WHERE id_servicio =" + String.valueOf(xid_servicio);
				stOrc.execute(sql);
				
				sql="INSERT INTO synergia.med_magnitud ("
					+"id_magnitud,  id_empresa,     id_componente,   id_servicio,     id_medida, "
					+"valor,        factor,         id_tip_magnitud, fec_lec_terreno, observaciones, "
					+"leido_desde,  fecha_lect_ini, id_tip_calculo,  id_est_magnitud, cod_observacion, "
					+"id_tipo_cons, sec_magnitud,   tipo,            enteros,         decimales ) "
					+"VALUES ( SQMAGNITUDIMPL.NEXTVAL,"
					+String.valueOf(pIDEmpr) + "," + String.valueOf(idcomponente) + "," + String.valueOf(pIDServicio) + "," 
					+String.valueOf(vIdMedida) + "," + vdValor + "," + vIdFactor + "," + String.valueOf(vIdTipMagnitud) + ","
					+"to_date('" + pFechaLectura +"', 'DD/MM/YYYY'), '', '', null, " + String.valueOf(vIdTipCalculo) + ","
					+vIdEstMagnitud + ", ''," + String.valueOf(vIdTipo_cons) + "," + String.valueOf(vIdSecMagnitud) + "," 
					+"'LECTURA'," + String.valueOf(vICantEnteros)+ ","+String.valueOf(vICantDecimales)+")";
				stOrc.execute(sql);
				
				sql="DELETE FROM schscom.med_lec_ultima WHERE id_componente ="+String.valueOf(idcomponente)
					+" AND id_medida = " + String.valueOf(vIdMedida) ;
				st.execute(sql);
				
				sql="INSERT INTO schscom.med_lec_ultima( id_lectura, id_empresa, id_medida, id_componente)"
					+ "VALUES ( nextval('schscom.sqmagnitudimpl'), "
					+ String.valueOf(pIDEmpr) + "," + String.valueOf(vIdMedida) + "," + String.valueOf(idcomponente) + ")";
				st.execute(sql);
						
			} //end for
			
			if(gNroSello1 > 0 || gNroSello2 > 0) {
				
				sql="UPDATE synergia.sel_sello SET"
					+"id_motivo_estado = "+String.valueOf(IdMotivoEstado)+", "
					+"fec_modificacion = current_timestamp, "
					+"fec_retiro = current_timestamp, "
					+"id_origen_ret = (select ID_ORIGEN from synergia.SEL_ORIGEN where COD_INTERNO='OrdenCorte'), "
					+"id_ejec_ret = (select ID_EJECUTOR from synergia.COM_EJECUTOR where COD_EJECUTOR = '' "+") "
					+"WHERE id_componente = "+String.valueOf(idcomponente);
				stOrc.execute(sql);
				
			}
			
			st.close();
//			BaseDatos.close();
			
			stOrc.close();
//			BaseDatosOrc.close();
			
			return true;
			
		} catch (SQLException ex) {
			System.out.println("Error en bfnRetirarMedidor:" + ex.getMessage());
			ex.printStackTrace(); /* ANL */
			st.close();
//			BaseDatos.close();
			stOrc.close();
//			BaseDatosOrc.close();
			return false;
		}
		
	}
	
	public boolean bfnInstalarMedidor(Connection pConORA, Connection pConPG, Integer pIDEmpr,
            Integer pIdUser,
            String nroMedRet,
            Integer piContReti,
            Long pidMedModelo,
            Integer pIDServicio,
            Long pnroCuenta,
            String gCodContratista,
            Integer gNroSello1,
			Integer gNroSello2,
			String gcolSello1,
			String gcolSello2,
			String gubiSello1,
			String gubiSello2,
			List<entLectura> arrLecturas,
			String pFechaLectura,
			Integer gCodMarMedInst,
			String gCodModMedInst
			,String pCodPropiedad
			// R.I. REQSCOM10 27/07/2023 INICIO
			,String gFactorMedInst
			// R.I. REQSCOM10 27/07/2023 FIN
			) throws SQLException
	{
//		Connection BaseDatos = null;
		Statement st = null;
//		Connection BaseDatosOrc = null;
		Statement stOrc = null;
		String sql = "";
		Integer idempresa=0;
		Integer IdMotivoEstado=0;
		Integer dynamic = 0;
		Integer idpropiedad = 0;
		//Long idcomponente = 12345678910L;
		Long idcomponente = 0L;
		Integer vIdTipMagnitud = 0;
		Integer vIdEstMagnitud = 0;
		String urlpg=""; String userpg=""; String passpg="";
		String urlOrc=""; String userOrc=""; String passOrc="";
		
//		urlOrc = conexOrc.getUrl();
//		userOrc = conexOrc.getUser();
//		passOrc = conexOrc.getPassword();
//		
//		urlpg = conexPg.getUrl();
//		userpg = conexPg.getUser();
//		passpg = conexPg.getPassword();
		
		try {
			
			// Empresa Global
			sql = "SELECT ne.id_empresa FROM schscom.nuc_empresa ne  WHERE ne.cod_partition = 'GLOB'";
//			BaseDatos = DriverManager.getConnection(urlpg, userpg, passpg);
			st = pConPG.createStatement();			
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				idempresa = rs.getInt("id_empresa");
				System.out.println(idempresa);				
			}
			
			// Ubicación/Servicio
			sql = "SELECT se.id_servicio FROM synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se"
					+" WHERE nc.id_cuenta = ns.id_cuenta AND ns.id_servicio = se.id_servicio"
					+" AND ns.tipo = 'ELECTRICO'" + "AND nc.nro_cuenta = " +String.valueOf(pnroCuenta);
//			BaseDatosOrc = DriverManager.getConnection(urlOrc, userOrc, passOrc);
			stOrc = pConORA.createStatement();
			ResultSet rsm = stOrc.executeQuery(sql);
			while (rsm.next()) {
				pIDServicio = rsm.getInt("id_servicio");
				System.out.println("pIDServicio:"+pIDServicio);				
			}
			
			
			// Estado Instalado
			sql = "SELECT id_motivo_estado FROM synergia.sel_motivo_estado 	WHERE cod_interno = 'Instalado' AND activo = 'S'";
			ResultSet rsi = stOrc.executeQuery(sql);
			while (rsi.next()) {
				IdMotivoEstado = rsi.getInt("id_motivo_estado");
				System.out.println("IdMotivoEstado:"+IdMotivoEstado);				
			}
			
			dynamic = -1;
			sql = "SELECT coalesce(id_dynamicobject,-1) dynamic, id_propiedad, id id_componente FROM schscom.med_componente "
					+" WHERE nro_componente = '" + nroMedRet + "' AND id_modelo = " + pidMedModelo ;
					//+" AND id_ubicacion = "+String.valueOf(pIDServicio);  /* ANL */ /* nroMedRet = gNroMedInst */
			ResultSet rsa = st.executeQuery(sql);
			while (rsa.next()) {
				dynamic = rsa.getInt("dynamic");
				idpropiedad = rsa.getInt("id_propiedad");
				idcomponente = rsa.getLong("id_componente");
				System.out.println("dynamic Ins:" + dynamic);
				System.out.println(idpropiedad);
				System.out.println(idcomponente);
			}
			
			Integer seqdynamic = 0;
			if (dynamic == -1) {
				
				sql = "SELECT nextval('schscom.sqdynamicbusinessobject') ID ";
				ResultSet rsb = st.executeQuery(sql);
				while (rsb.next()) {
					seqdynamic = rsb.getInt("ID");
					System.out.println("seqdynamic Ins: " + seqdynamic);
				}
				sql = "INSERT INTO schscom.DYO_OBJECT(ID) VALUES(" + String.valueOf(seqdynamic) + ")";
				st.execute(sql);
			}
			
			
			Integer idcliente = 0;
			//sql = "Select cli.id_cliente from synergia.nuc_cliente cli, synergia.nuc_cuenta nc where cli.id_cliente = nc.id_cliente and nc.nro_cuenta = "+ String.valueOf(pnroCuenta);
			sql = "select c.id_servicio from schscom.cliente c where c.nro_cuenta = " + String.valueOf(pnroCuenta); /* ANL */			
			ResultSet rsj = st.executeQuery(sql);
			while (rsj.next()) {
				idcliente = rsj.getInt("id_servicio");
				System.out.println(idcliente);
			}

			
/*			sql = "UPDATE schscom.med_componente ";
			sql += " SET id_est_componente = (select id from schscom.med_est_componente where cod_interno = 'Instalado'), ";
			sql += " id_ubicacion = (case when id_propiedad = 25 then " + String.valueOf(idcliente);
			sql += " else (select id from schscom.com_contratista ";
			sql += " where cod_contratista = '" + String.valueOf(gCodContratista) + "' ) end), ";
			sql += " type_ubicacion = (case when id_propiedad = 25 ";
			sql += " then 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente' ";
			sql += " else 'com.synapsis.synergia.common.domain.Contratista' ";
			sql += " end) ";
*/
			sql = "UPDATE schscom.med_componente ";  /* ANL */
			sql += " SET id_est_componente = (select id from schscom.med_est_componente where cod_interno = 'Instalado') ";
			sql += "    ,id_ubicacion      = " + String.valueOf(idcliente);
			sql += "    ,type_ubicacion   = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'";
			sql += "    ,id_propiedad     = (select id from schscom.med_propiedad where cod_propiedad = '" + pCodPropiedad + "')";
			sql += "    ,fecha_suministro = to_date('" + pFechaLectura + "','dd/mm/yyyy')"; 
			if(dynamic == -1) {
				sql += " ,id_dynamicobject = " + String.valueOf(seqdynamic);
			}
			sql += " WHERE nro_componente = '" + nroMedRet + "' ";
			sql += "   AND id_modelo = " + pidMedModelo;

			//System.out.println(sql);
			st.execute(sql);
			
			Long idhiscomponente = null;
			sql = "SELECT coalesce(max(id_his_componente),0) ID FROM schscom.med_his_componente  WHERE id_componente = "+String.valueOf(idcomponente);// + " AND id_est_componente = 18";
			ResultSet rsb = st.executeQuery(sql);
			while (rsb.next()) {
				idhiscomponente = rsb.getLong("ID");
				System.out.println("idhiscomponente Ins: "+idhiscomponente);
			}
			
			sql = "UPDATE schscom.med_his_componente ";
			sql += "SET fec_hasta = to_date('" + pFechaLectura + "','dd/mm/yyyy')"; /* ANL */
			sql += "WHERE id_his_componente = " + String.valueOf(idhiscomponente);
			st.execute(sql);
			
			Long seqauditevent = null;
			sql = "SELECT nextval('schscom.sqauditevent') ID ";
			ResultSet rsk = st.executeQuery(sql);
			while (rsk.next()) {
				seqauditevent = rsk.getLong("ID");
				System.out.println("seqauditeventIns: " + seqauditevent);
			}
			
/*			Integer idcliente2 = 0;
			sql = "Select cli.id_cliente from synergia.nuc_cliente cli, synergia.nuc_cuenta nc where cli.id_cliente = nc.id_cliente and nc.nro_cuenta = "+ String.valueOf(pnroCuenta);
			ResultSet rsy = stOrc.executeQuery(sql);
			while (rsy.next()) {
				idcliente2 = rsy.getInt("id_cliente");
				System.out.println("idcliente2 Ins: " +idcliente2);
			}
*/
			//Insertando med_his_componente  pIDServicio
			sql = "INSERT INTO schscom.MED_HIS_COMPONENTE(id_his_componente, id_componente, id_est_componente, fec_desde, id_ubicacion, type_ubicacion) " 
				+ " Select " + String.valueOf(seqauditevent) + "," +  String.valueOf(idcomponente) + "," 
				+ " (Select id from schscom.med_est_componente where cod_interno = 'Instalado'), to_date('" + pFechaLectura +"', 'DD/MM/YYYY'), "
				+ pIDServicio.toString() + " , "
				+ " 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "; /* ANL */
			st.execute(sql);
			
			//Insertando auditoria
			sql = "INSERT INTO schscom.FWK_AUDITEVENT(id, usecase, objectref, id_fk, fecha_ejecucion, specific_auditevent, id_user) ";
			sql += "VALUES( "+ String.valueOf(seqauditevent) + ",'Medidor.UPDATE', 'com.synapsis.synergia.med.domain.componente.Medidor', ";
			sql += String.valueOf(idcomponente) + ", current_timestamp, 'COMPONENTE', "+ pIdUser + ") ";
			st.execute(sql);

//			sql = "SELECT id_tip_magnitud ID FROM schscom.med_tip_magnitud WHERE cod_interno = 'Retiro' ";
			sql = "SELECT id_tip_magnitud ID FROM schscom.med_tip_magnitud WHERE cod_interno = 'Instalacion' "; /* ANL */
			ResultSet rst = st.executeQuery(sql);
			while (rst.next()) {
				vIdTipMagnitud = rst.getInt("ID");
				System.out.println("vIdTipMagnitud Ins: "+vIdTipMagnitud);
			}
			
			sql = "SELECT id_est_magnitud ID FROM schscom.fac_est_magnitud WHERE codigo = '01' ";
			ResultSet rsh = st.executeQuery(sql);
			while (rsh.next()) {
				vIdEstMagnitud = rsh.getInt("ID");
				System.out.println("vIdEstMagnitud Ins: "+vIdEstMagnitud);
			}
			System.out.println("vidcomponente: "+idcomponente);
			System.out.println("gFactorMedInst: "+gFactorMedInst);
			System.out.println("pidMedModelo: "+pidMedModelo);
			
			Integer vIdMedida=0;
			String vIdFactor="";
			Integer vIdTipCalculo=0;
			Integer vIdTipo_cons=0;
			Integer vICantEnteros=0;
			Integer vICantDecimales=0;
			Integer vIdSecMagnitud=0;
			Integer xid_servicio=0;
			
			for(int i = 0; i < piContReti; i++) {
				
				String vCodigo = "";
				String vdValor = "";
				vCodigo = arrLecturas.get(i).getCodigo();
				vdValor = arrLecturas.get(i).getValor();
				System.out.println("Actualizando lectura nro " + (i + 1));
				System.out.println("idcomponente: "+idcomponente);
				System.out.println("vCodigo: "+vCodigo);
				System.out.println("vdValor: "+vdValor);
				sql = "SELECT med.id_medida, fac.val_factor, med.id_tip_calculo, tc.id_tipo_cons, med.cant_enteros, med.cant_decimales "
						+" FROM schscom.med_medida me, schscom.med_medida_medidor med, schscom.med_factor fac, schscom.med_ent_dec ed, schscom.fac_tipo_consumo tc "
						+" WHERE med.id_componente = "+ String.valueOf(idcomponente) 
						+ " AND me.id = med.id_medida "
						+" AND med.id_factor = fac.id "
						+" AND med.id_ent_dec = ed.id "
						+" AND med.id_medida = tc.id_medida "
						+" AND me.cod_medida = '" + vCodigo + "' ";
				try {
					ResultSet rsp = st.executeQuery(sql);
					vIdFactor = "''";
					System.out.println(rsp.getFetchSize());
					while (rsp.next()) {
						vIdMedida = rsp.getInt("id_medida");
						vIdFactor = rsp.getString("val_factor");
						vIdTipCalculo = rsp.getInt("id_tip_calculo");
						vIdTipo_cons = rsp.getInt("id_tipo_cons");
						vICantEnteros = rsp.getInt("cant_enteros");
						vICantDecimales = rsp.getInt("cant_decimales");
						System.out.println("vIdMedida Ins:"+vIdMedida);
						System.out.println("vIdFactor Ins:"+vIdFactor);
						System.out.println("vIdTipCalculo Ins:"+vIdTipCalculo);
					}
					
				} catch (SQLException ex) {
					st.close();
					//BaseDatos.close();
					System.err.println("Error, no se encontró información en 'qRet6'. Cuenta : " + String.valueOf(pnroCuenta) + "Revisar Log del Proceso");
					return false;
				}
				
				sql = "SELECT se.sec_magnitud + 1 magnitud, se.id_servicio "
						+"FROM synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se "
						+"WHERE nc.id_cuenta = ns.id_cuenta "
						+"AND ns.id_servicio = se.id_servicio "
						+"AND ns.tipo = 'ELECTRICO' "
						+"AND nc.nro_cuenta = " + String.valueOf(pnroCuenta) ;
				try {
					ResultSet rsd = stOrc.executeQuery(sql);
					while (rsd.next()) {
						vIdSecMagnitud = rsd.getInt("magnitud");
						xid_servicio = rsd.getInt("id_servicio");
						System.out.println("vIdSecMagnitud Ins:"+vIdSecMagnitud);
						System.out.println("xid_servicio Ins:"+xid_servicio);
					}
					
				} catch (SQLException ex) {
					st.close();
//					BaseDatos.close();
					System.err.println("Error en bfnRetirarMedidor_Into_vIdSecMagnitud. Cuenta : " + String.valueOf(pnroCuenta) + "Revisar Log del Proceso");
					return false;
				}
				
				sql = "UPDATE synergia.srv_electrico "
					+"SET sec_magnitud = " + String.valueOf(vIdSecMagnitud)
					+"WHERE id_servicio =" + String.valueOf(xid_servicio);
				stOrc.execute(sql);
				
				System.out.println("vIdMedida: "+vIdMedida);
				
				sql="INSERT INTO synergia.med_magnitud ("
					+"id_magnitud,  id_empresa,     id_componente,   id_servicio,     id_medida, "
					+"valor,        factor,         id_tip_magnitud, fec_lec_terreno, observaciones, "
					+"leido_desde,  fecha_lect_ini, id_tip_calculo,  id_est_magnitud, cod_observacion, "
					+"id_tipo_cons, sec_magnitud,   tipo,            enteros,         decimales ) "
					+"VALUES ( SQMAGNITUDIMPL.NEXTVAL,"
					+String.valueOf(pIDEmpr) + "," + String.valueOf(idcomponente) + "," + String.valueOf(pIDServicio) + "," 
					+String.valueOf(vIdMedida) + "," + vdValor + "," + vIdFactor + "," + String.valueOf(vIdTipMagnitud) + ","
					+"to_date('" + pFechaLectura +"', 'DD/MM/YYYY'), '', '', null, " + String.valueOf(vIdTipCalculo) + ","
					+vIdEstMagnitud + ", ''," + String.valueOf(vIdTipo_cons) + "," + String.valueOf(vIdSecMagnitud) + "," 
					+"'LECTURA'," + String.valueOf(vICantEnteros)+ ","+String.valueOf(vICantDecimales)+")";
				stOrc.execute(sql);
				
				sql="DELETE FROM schscom.med_lec_ultima WHERE id_componente ="+String.valueOf(idcomponente)
					+" AND id_medida = " + String.valueOf(vIdMedida) ;
				st.execute(sql);
				
				sql="INSERT INTO schscom.med_lec_ultima( id_lectura, id_empresa, id_medida, id_componente)"
					+ "VALUES ( nextval('schscom.sqmagnitudimpl'), "
					+ String.valueOf(pIDEmpr) + "," + String.valueOf(vIdMedida) + "," + String.valueOf(idcomponente) + ")";
				st.execute(sql);
						
			} //end for
			
						
			Long idColor1=null;
			Long idBolsaSello1=null;
			String cDesSello1="";
			if(gNroSello1 > 0) {
				
				sql = "SELECT des_color, id_color "
						+" FROM synergia.sel_color "
						+" WHERE activo = 'S' AND cod_color = '" + gcolSello1 + "'";
				ResultSet rsc = stOrc.executeQuery(sql);
				while (rsc.next()) {
					cDesSello1 = rsc.getString("des_color");
					idColor1 = rsc.getLong("id_color");
					System.out.println("cDesSello1:"+cDesSello1);
					System.out.println("idColor:"+idColor1);
				}
				
				sql = "SELECT sel.id_bolsa_sellos "
						+" FROM synergia.sel_sello sel, synergia.sel_bolsa_sellos bol "
						+" WHERE sel.id_motivo_estado = 1 AND sel.id_bolsa_sellos  = bol.id_bolsa_sellos AND sel.nro_sello = " + String.valueOf(gNroSello1);
				ResultSet rsba = stOrc.executeQuery(sql);
				while (rsba.next()) {
					idBolsaSello1 = rsba.getLong("id_bolsa_sellos");
					System.out.println("idBolsaSello1:"+idBolsaSello1);
				}
				
				sql="UPDATE synergia.sel_sello SET"
					+" id_motivo_estado ="+String.valueOf(IdMotivoEstado)+", "
					+" fec_instalacion =sysdate, " /* ANL */
					+" id_origen_inst   = (select id_origen from synergia.sel_origen where cod_origen = 'ADM'), "
					+" fec_modificacion =sysdate, " /* ANL */
					+" id_ubicacion  = (select id_ubicacion from synergia.sel_ubicacion where cod_ubicacion = '" + gubiSello1 + "' And activo = 'S'), "
					+ "id_servicio =" +  String.valueOf(pIDServicio) +", "
					+" id_componente =" +  String.valueOf(idcomponente) +", "
					+" id_ejec_inst   = (select id_ejecutor from synergia.com_ejecutor where cod_ejecutor = '') " //cCodEjecutor
					+" WHERE nro_sello = "+String.valueOf(gNroSello1)
					+" AND id_motivo_estado = 1"
					+" AND id_bolsa_sellos = "+String.valueOf(idBolsaSello1);
				stOrc.execute(sql);
				
				sql = "INSERT INTO SYNERGIA.SEL_HIS_SELLO ( " +						
						"id_his_sello, " +
						"id_empresa, " +
						"serie, " +
						"color, " +
						"nro_sello, " +
						"estado_sello, " +
						"contratista_inst, " +
						"ejecutor_inst, " +
						"ubicacion, " +
						"srv_electrico, " +
						"tip_componente, " +
						"marca_componente, " +
						"modelo_componente, " +
						"fec_creacion, " +
						"fec_instalacion " +
						")" +
						" VALUES( SQHISTORIASELLO.NEXTVAL," +
						String.valueOf(idempresa) + ", " +
						" (select serie from synergia.sel_bolsa_sellos WHERE id_bolsa_sellos = " + 
						String.valueOf(idBolsaSello1) + "), '" + cDesSello1 + "', " + String.valueOf(gNroSello1) + " , " + 
						" (select des_est_sello from synergia.sel_est_sello where cod_interno = 'Instalado'), " + 
						" (select con.cod_contratista || ' ' || trim(np.nombre) from synergia.com_contratista con, synergia.nuc_persona np " +
						" where np.id_persona = con.id_persona and con.cod_contratista = '" + gCodContratista + "'), " +
						" (select  trim(np.nombre) || ' ' || trim(np.apellido_pat) || ' ' || trim(np.apellido_mat) from synergia.com_ejecutor eje, synergia.nuc_persona np " +
						" where np.id_persona = eje.id_persona and eje.cod_ejecutor = '' ), " +
						" (select des_ubicacion from synergia.sel_ubicacion where cod_ubicacion = '" + gubiSello1 + "' And activo = 'S')," + 
						" (select 'Servicio Nro:' || ' ' || ns.nro_servicio from synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se " +
						" where nc.id_cuenta = ns.id_cuenta and ns.id_servicio = se.id_servicio  and ns.tipo = 'ELECTRICO'  and nc.nro_cuenta = "+String.valueOf(pnroCuenta) + "), " +
						" (select des_tip_componente from synergia.med_tip_componente where cod_tip_componente = 'ME'), "+
						" (select des_marca from synergia.med_marca where cod_marca = '" + gCodMarMedInst +"' and activo = 'S'), " +
						" (select mo.des_modelo from med_modelo mo, med_marca ma " +
						" where mo.id_marca = ma.id_marca and mo.cod_modelo = '"+ gCodModMedInst + "' and mo.activo = 'S' and ma.cod_marca = '" + gCodMarMedInst + "' and ma.activo = 'S'), " +
						" (select s.fec_creacion from synergia.sel_bolsa_sellos bs, synergia.sel_sello s " + 
						" where s.nro_sello = "+String.valueOf(gNroSello1)+" and bs.id_bolsa_sellos = s.id_bolsa_sellos " +
						" and bs.id_color = " +String.valueOf(idColor1)+ " and bs.id_bolsa_sellos = " + String.valueOf(idBolsaSello1) + " ), current_timestamp )";
				stOrc.execute(sql);	
						
				Integer regSello = 0;		
				sql = "SELECT count(1) total "
						+" FROM synergia.sel_sello sel "
						+" WHERE id_bolsa_sellos = " + String.valueOf(idBolsaSello1)
						+" AND id_motivo_estado IN (select id_motivo_estado from synergia.sel_motivo_estado where cod_interno in ('Creado','Disponible')) ";
				ResultSet rsbd = stOrc.executeQuery(sql);
				while (rsbd.next()) {
					regSello = rsbd.getInt("total");
					System.out.println("regSello:"+regSello);
				}
				
				if (regSello == 0) {
					sql= "UPDATE synergia.sel_bolsa_sellos "+
						" SET id_estado   = (select ID_EST_BOLSA from synergia.SEL_EST_BOLSA where COD_INTERNO='Terminada'), "+
						" id_ejecutor = (select  eje.id_ejecutor from synergia.com_ejecutor eje, synergia.nuc_persona np "+
						" where np.id_persona = eje.id_persona and eje.cod_ejecutor = ''), "+
						" fec_termino=sysdate"+  
						" WHERE id_bolsa_sellos ="+String.valueOf(idBolsaSello1);
					stOrc.execute(sql);
				}
			} //end if gNroSello1
			
			Long idColor2=null;
			Long idBolsaSello2=null;
			String cDesSello2="";
			if(gNroSello2 > 0) {
				
				sql = "SELECT des_color, id_color "
						+" FROM synergia.sel_color "
						+" WHERE activo = 'S' AND cod_color = '" + gcolSello2 + "'";
				ResultSet rsjc = stOrc.executeQuery(sql);
				while (rsjc.next()) {
					cDesSello2 = rsjc.getString("des_color");
					idColor2 = rsjc.getLong("id_color");
					System.out.println("cDesSello2:"+cDesSello2);
					System.out.println("idColor2:"+idColor2);
				}
				
				sql = "SELECT sel.id_bolsa_sellos "
						+" FROM synergia.sel_sello sel, synergia.sel_bolsa_sellos bol "
						+" WHERE sel.id_motivo_estado = 1 AND sel.id_bolsa_sellos  = bol.id_bolsa_sellos AND sel.nro_sello = " + String.valueOf(gNroSello2);
				ResultSet rsba = stOrc.executeQuery(sql);
				while (rsba.next()) {
					idBolsaSello2 = rsba.getLong("id_bolsa_sellos");
					System.out.println("idBolsaSello2:"+idBolsaSello2);
				}
				
				sql="UPDATE synergia.sel_sello SET"
					+" id_motivo_estado ="+String.valueOf(IdMotivoEstado)+", "
					+" fec_instalacion = sysdate, " /* ANL */
					+" id_origen_inst   = (select id_origen from synergia.sel_origen where cod_origen = 'ADM'), "
					+" fec_modificacion = sysdate, " /* ANL */
					+" id_ubicacion     = (select id_ubicacion from synergia.sel_ubicacion where cod_ubicacion = '" + gubiSello2 + "' and activo = 'S'), "
					+" id_servicio = " +  String.valueOf(pIDServicio) + ","
					+" id_componente = " +  String.valueOf(idcomponente) + ","
					+" id_ejec_inst  = (select id_ejecutor from synergia.com_ejecutor where cod_ejecutor = '') " //cCodEjecutor
					+" WHERE nro_sello = "+String.valueOf(gNroSello2)
					+" AND id_motivo_estado = 1"
					+" AND id_bolsa_sellos = "+String.valueOf(idBolsaSello2);
				stOrc.execute(sql);
				
				sql = "INSERT INTO SYNERGIA.SEL_HIS_SELLO ( " +						
						"id_his_sello, " +
						"id_empresa, " +
						"serie, " +
						"color, " +
						"nro_sello, " +
						"estado_sello, " +
						"contratista_inst, " +
						"ejecutor_inst, " +
						"ubicacion, " +
						"srv_electrico, " +
						"tip_componente, " +
						"marca_componente, " +
						"modelo_componente, " +
						"fec_creacion, " +
						"fec_instalacion " +
						")" +
						" VALUES( SQHISTORIASELLO.NEXTVAL," +
						String.valueOf(idempresa) + ", " +
						" (select serie from synergia.sel_bolsa_sellos WHERE id_bolsa_sellos = " + 
						String.valueOf(idBolsaSello2) + "), '" + cDesSello2 + "', " + String.valueOf(gNroSello2) + " , " + 
						" (select des_est_sello from synergia.sel_est_sello where cod_interno = 'Instalado'), " + 
						" (select con.cod_contratista || ' ' || trim(np.nombre) from synergia.com_contratista con, synergia.nuc_persona np " +
						" where np.id_persona = con.id_persona and con.cod_contratista = '" + gCodContratista + "'), " +
						" (select  trim(np.nombre) || ' ' || trim(np.apellido_pat) || ' ' || trim(np.apellido_mat) from synergia.com_ejecutor eje, synergia.nuc_persona np " +
						" where np.id_persona = eje.id_persona and eje.cod_ejecutor = '' ), " +
						" (select des_ubicacion from synergia.sel_ubicacion where cod_ubicacion = '" + gubiSello2 + "' And activo = 'S')," + 
						" (select 'Servicio Nro:' || ' ' || ns.nro_servicio from synergia.nuc_cuenta nc, synergia.nuc_servicio ns, synergia.srv_electrico se " +
						" where nc.id_cuenta = ns.id_cuenta and ns.id_servicio = se.id_servicio  and ns.tipo = 'ELECTRICO'  and nc.nro_cuenta = "+String.valueOf(pnroCuenta) + "), " +
						" (select des_tip_componente from synergia.med_tip_componente where cod_tip_componente = 'ME'), "+
						" (select des_marca from synergia.med_marca where cod_marca = '" + gCodMarMedInst +"' and activo = 'S'), " +
						" (select mo.des_modelo from med_modelo mo, med_marca ma " +
						" where mo.id_marca = ma.id_marca and mo.cod_modelo = '"+ gCodModMedInst + "' and mo.activo = 'S' and ma.cod_marca = '" + gCodMarMedInst + "' and ma.activo = 'S'), " +
						" (select s.fec_creacion from synergia.sel_bolsa_sellos bs, synergia.sel_sello s " + 
						" where s.nro_sello = "+String.valueOf(gNroSello2)+" and bs.id_bolsa_sellos = s.id_bolsa_sellos " +
						" and bs.id_color = " +String.valueOf(idColor2)+ " and bs.id_bolsa_sellos = " + String.valueOf(idBolsaSello2) + " ), sysdate )"; /* ANL */
				stOrc.execute(sql);	
						
				Integer regSello = 0;		
				sql = "SELECT count(1) total "
						+"FROM synergia.sel_sello sel "
						+"WHERE id_bolsa_sellos = " + String.valueOf(idBolsaSello2)
						+"AND id_motivo_estado IN (select id_motivo_estado from synergia.sel_motivo_estado where cod_interno in ('Creado','Disponible')) ";
				ResultSet rsbd = stOrc.executeQuery(sql);
				while (rsbd.next()) {
					regSello = rsbd.getInt("total");
					System.out.println("regSello:"+regSello);
				}
				
				if (regSello == 0) {
					sql= "UPDATE synergia.sel_bolsa_sellos "+
						"SET id_estado = (select ID_EST_BOLSA from synergia.SEL_EST_BOLSA where COD_INTERNO='Terminada'), "+
						"id_ejecutor = (select  eje.id_ejecutor from synergia.com_ejecutor eje, synergia.nuc_persona np "+
						"where np.id_persona = eje.id_persona and eje.cod_ejecutor = ''), "+
						"fec_termino=sysdate"+
						"WHERE id_bolsa_sellos ="+String.valueOf(idBolsaSello2);
					stOrc.execute(sql);
				}
			} //end if gNroSello2
			
			stOrc.close();
//			BaseDatosOrc.close(); /* ANL */
			st.close();
//			BaseDatos.close(); /* ANL */
			
			return true;
			
		} catch (SQLException ex) {
			System.out.println("Error en bfnInstalarMedidor:" + ex.getMessage());
			ex.printStackTrace(); /* ANL */
			stOrc.close();
//			BaseDatosOrc.close();
			st.close();
//			BaseDatos.close();
			return false;
		}
	}
	
	/***/
	public entResultadoStr bfnGenerarORM(Connection pConPG, Integer pEntServElect, Integer pUser, entParamORM pParamOrm) throws Exception {
		
		entResultadoStr objStrLog = new entResultadoStr();
		String sSQL = "";
		
		Statement stPg = null;
		
		Long iIdWorkflowH = 0L;
		Long lNroOrdenH=0L;
		Long iIdOrdenH=0L;
		
		try {
			
			stPg = pConPG.createStatement();
			
			/* Workflow */
			sSQL = "SELECT nextval('schscom.sqworkflow') as id";
			try {
				ResultSet rs = stPg.executeQuery(sSQL);
				while (rs.next()) {
					iIdWorkflowH = rs.getLong("id");				
				}
				rs.close();
			}catch(Exception e){
				throw new Exception(String.format("Error en SELECT_nextval_sqworkflow. [%s]", e.getMessage()));	
			}
			
			sSQL = "INSERT INTO schscom.wkf_workflow ( \n"
				+ "    id \n"
				+ "    ,id_descriptor \n"
				+ "    ,version \n"
				+ "    ,id_state \n"
				+ "    ,has_parent \n"
				+ "    ,parent_type \n"
				+ "    ,id_old_state \n"
				+ "    ) \n"
				+ "VALUES ( \n"
				+ "    " + iIdWorkflowH
				+ "    ,'ordenmantenimientoWF' \n"
				+ "    ,0 \n"
				+ "    ,'Finalizada' \n"
				+ "    ,'S' \n"
				+ "    ,'com.synapsis.synergia.orm.domain.OrdenMantenimiento' \n"
				+ "    ,'Finalizada' \n"
				+ "    ) ";			
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_wkf_workflow. [%s]", e.getMessage()));	
			}
			
			/* ord_orden */
			sSQL = "SELECT nextval('schscom.sqordenmantenimiento') as id";
			try {
				ResultSet rs = stPg.executeQuery(sSQL);
				while (rs.next()) {
					lNroOrdenH = rs.getLong("id");				
				}
				rs.close();  
			}catch(Exception e){
				throw new Exception(String.format("Error en SELECT_nextval_sqordenmantenimiento. [%s]", e.getMessage()));	
			}
			
			sSQL = "SELECT nextval('schscom.sqorden') as id";
			try {
				ResultSet rs = stPg.executeQuery(sSQL);
				while (rs.next()) {
					iIdOrdenH = rs.getLong("id");				
				}
				rs.close();  
			}catch(Exception e){
				throw new Exception(String.format("Error en SELECT_nextval_sqorden. [%s]", e.getMessage()));	
			}
			
			sSQL = "INSERT INTO schscom.ord_orden ( \n"
				+ "    nro_orden \n"
				+ "    ,id_workflow \n"
				+ "    ,id_tipo_orden \n"
				+ "    ,id \n"
				+ "    ,id_motivo \n"
				+ "    ,fecha_ingreso_estado_actual \n"
				+ "    ,fecha_creacion \n"
				+ "    ,discriminador \n"
				+ "    ,id_servicio \n"
				+ "    ,id_usuario_creador \n"
				+ "    ,id_buzon \n"
				+ "    ,fecha_vencimiento \n"
				+ "    ) \n"
				+ "VALUES ( \n"
				+ "    " + lNroOrdenH.toString() + "\n"
				+ "    ," + iIdWorkflowH.toString() + "\n"
				+ "    ,2 \n"
				+ "    ," + iIdOrdenH.toString() + "\n"
				+ "    ," + pParamOrm.getId_motivo() + "\n"    /* :iIdMotivoH  - ANL_PARAMETRIZAR */
				+ "    ,now() \n"
				+ "    ,now() \n"
				+ "    ,'ORDEN_MANTENIMIENTO' \n"
				+ "    ," + pEntServElect.toString() + "\n"
				+ "    ," + pUser.toString() + "\n"
				+ "    ," +  pParamOrm.getId_buzon() + "\n"    /* :iIdBuzonH  ANL_PARAMETRIZAR */
				+ "    ,now() \n"
				+ "    ) ";
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_ord_orden. [%s]", e.getMessage()));	
			}

			/* orm_orden */
			sSQL = "INSERT INTO schscom.orm_orden ( \n"
				+ "    id_orden \n"
				+ "    ,id_trabajo \n"
				+ "    ,id_prioridad \n"
				+ "    ,id_tema \n"
				+ "    ,tipo_creacion \n"
				+ "    ,cod_proceso \n"
				+ "    ) \n"
				+ "VALUES ( \n"
				+ "    " + iIdOrdenH.toString() + "\n"
				+ "    ," + pParamOrm.getId_trabajo() + "\n"
				+ "    ," + pParamOrm.getId_prioridad() + "\n"
				+ "    ," + pParamOrm.getId_tema() + "\n"
				+ "    ,'Masiva' \n"
				+ "    ,'' \n"
				+ "    ) ";
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_orm_orden. [%s]", e.getMessage()));	
			}
			
			/* orm_tarea_ejec */
			sSQL = "INSERT INTO schscom.orm_tarea_ejec ( \n"
					+ "    id \n"
					+ "    ,id_tarea \n"
					+ "    ,id_Orden \n"
					+ "    ,fecha_Creacion \n"
					+ "    ) \n"
					+ "VALUES ( \n"
					+ "    nextval('schscom.sqtareaejecutadaordenmantenimi') \n"
					+ "    ," + pParamOrm.getId_tarea() + "\n"
					+ "    ," + iIdOrdenH.toString() + "\n"
					+ "    ,now() \n"
					+ "    ) ";
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_orm_tarea_ejec. [%s]", e.getMessage()));	
			}		
					
			
			/* ord_ord_deriv */
			sSQL = "INSERT INTO schscom.ord_ord_deriv ( \n"
				+ "    id_orden \n"
				+ "    ,id_Responsable \n"
				+ "    ,id_ult_responsable \n"
				+ "    ) \n"
				+ "VALUES ( \n"
				+ "     " + iIdOrdenH.toString() + "\n"
				+ "    ," +  pParamOrm.getId_responsable() + "\n"
				+ "    ," +  pParamOrm.getId_responsable() + "\n"
				+ "    ) ";
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_ord_ord_deriv. [%s]", e.getMessage()));	
			}
			
			/* ord_observacion */
			sSQL = "INSERT INTO schscom.ord_observacion ( \n"
				+ "    id_observacion \n"
				+ "    ,id_orden \n"
				+ "    ,fecha_observacion \n"
				+ "    ,id_usuario \n"
				+ "    ,texto \n"
				+ "    ,state_name \n"
				+ "    ,discriminator \n"
				+ "    ) \n"
				+ "VALUES ( \n"
				+ "    nextval('schscom.sqobservacionorden') \n"
				+ "    ," + iIdOrdenH.toString() + "\n"
				+ "    ,now() \n"
				+ "    ," + pUser.toString() + "\n"
				+ "    ,'cadena cadena' \n"
				+ "    ,'Emitida' \n"
				+ "    ,'ObservacionOrden' \n"
				+ "    ) ";
			try {
				stPg.execute(sSQL);	
			}catch(Exception e){
				throw new Exception(String.format("Error en INSERT_INTO_ord_observacion. [%s]", e.getMessage()));	
			}
			
			stPg.close();
			
			objStrLog.setResult(true);
			
		} catch (Exception e) {
			objStrLog.setCampo("Error: " + e.getMessage());
			objStrLog.setResult(false);
			e.printStackTrace();
		}
	
		return objStrLog;
	}
	
	/***/
	public Integer bfnParamValorNum(Connection pConPG, String pCodigo) throws Exception {	
	
		String sSQL="";
		Integer ValorNum=0;
		Statement stPg = null;

		
		try {
			stPg = pConPG.createStatement();
			
			sSQL = "SELECT valor_num \n"
					+ " FROM schscom.com_parametros cp \n"
					+ "WHERE sistema = 'SCOM' \n"
					+ "  AND entidad = 'ORM_GENERICA' \n"
					+ "  AND codigo = '" + pCodigo + "'" ;
			
			ResultSet rs = stPg.executeQuery(sSQL);
			while (rs.next()) {
				ValorNum = rs.getInt("valor_num");				
			}
			rs.close();
			stPg.close();
					
			return ValorNum;
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(String.format("Error en bfnParamValorNum. [%s]", e.getMessage()));
		}
	}
	
	/***/
	public String bfnParamValorAlf(Connection pConPG, String pCodigo) throws Exception {	
	
		String sSQL="";
		String ValorAlf="";
		Statement stPg = null;

		
		try {
			stPg = pConPG.createStatement();
			
			sSQL = "SELECT valor_alf \n"
					+ " FROM schscom.com_parametros cp \n"
					+ "WHERE sistema = 'SCOM' \n"
					+ "  AND entidad = 'ORM_GENERICA' \n"
					+ "  AND codigo = '" + pCodigo + "'" ;
			
			ResultSet rs = stPg.executeQuery(sSQL);
			while (rs.next()) {
				ValorAlf = rs.getString("valor_alf");				
			}
			rs.close();
			stPg.close();
					
			return ValorAlf;
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(String.format("Error en bfnParamValorAlf. [%s]", e.getMessage()));
		}
	}

	/***/
	public entParamORM CargarParametrosORM(Connection pConPg) throws Exception  {
		
		entParamORM pParamOrm = new entParamORM();
		
		try {
			pParamOrm.setId_motivo(bfnParamValorNum(pConPg, "ID_MOTIVO"));
			pParamOrm.setId_buzon(bfnParamValorNum(pConPg, "ID_BUZON"));
			pParamOrm.setId_trabajo(bfnParamValorNum(pConPg, "ID_TRABAJO"));
			pParamOrm.setId_prioridad(bfnParamValorNum(pConPg, "ID_PRIORI"));
			pParamOrm.setId_tema(bfnParamValorNum(pConPg, "ID_TEMA"));
			pParamOrm.setId_responsable(bfnParamValorNum(pConPg, "ID_RESPONS"));
			pParamOrm.setId_tarea(bfnParamValorNum(pConPg, "ID_TAREA"));
			pParamOrm.setObservacion(bfnParamValorAlf(pConPg, "OBSERVA"));
			return pParamOrm;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(String.format("Error en CargarParametrosORM. [%s]", e.getMessage()));
		}
	}
	
	/***/
    public String Ruta_Directorio_SFPT(entConexion conexPg) throws Exception{
    	
    	String ruta = "";
    	Connection BaseDatos = null;
		Statement st = null;
    	
    	try {
    		
    		BaseDatos = DriverManager.getConnection(conexPg.getUrl(), conexPg.getUser(), conexPg.getPassword());
			st = BaseDatos.createStatement();
    		
    		String sSQL = "select valor_alf \n"
    				+ " from schscom.com_parametros \n"
    				+ " where sistema ='EORDER' \n"
    				+ " and entidad = 'SFTP_ODT' \n"
    				+ " and codigo='PATH_MED' ";
    		
    		ResultSet rs = st.executeQuery(sSQL);
			while (rs.next()) {
				ruta += rs.getString("valor_alf");
			}
			
			rs.close(); 
			BaseDatos.close();
			
    		
		}catch (Exception e){
	    	e.printStackTrace();
	    	System.err.println(String.format("Error en Ruta_Directorio_SFPT. Ruta: [%d] [%s]", ruta, e.getMessage()));
		}
    	
    	return ruta;
    }
    
    
    /***/
    public String Activo_SFPT(entConexion conexPg) throws Exception{
    	
    	String valor = "";
    	Connection BaseDatos = null;
		Statement st = null;
		
    	try {
    		
    		BaseDatos = DriverManager.getConnection(conexPg.getUrl(), conexPg.getUser(), conexPg.getPassword());
			st = BaseDatos.createStatement();
			
    		String sSQL = "select valor_alf \n"
    				+ " from schscom.com_parametros \n"
    				+ " where sistema ='EORDER' \n"
    				+ " and entidad = 'SFTP_ODT' \n"
    				+ " and codigo='ACTV_SFTP' "; 
			
    		ResultSet rs = st.executeQuery(sSQL);
			while (rs.next()) {
				valor += rs.getString("valor_alf");
			}
			
			rs.close(); 
			BaseDatos.close();
				
		}catch (Exception e){
	    	e.printStackTrace();
	    	System.err.println(String.format("Error en Activo_SFPT. Valor: [%d] [%s]", valor, e.getMessage()));
		} 
    	
    	return valor;
    }



}
